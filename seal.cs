using System;
namespace seal {
	sealed class A {
		public void Write() {
			Console.WriteLine("A");
		}
	}

	//Error, A is sealed, only can be instance 
	class B {
		public void Write2() {
			Console.WriteLine("B");
		}
	}

	class Seal {
		public static void Main(string[] args) {
			B b = new B();
			b.Write2();

			A a = new A();
			a.Write();
		}
	}
}
