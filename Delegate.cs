using System;

namespace delage{
	class Delegate {
		delegate int deal(int a, int b);
		static int func1(int a, int b) {
			return a + b;
		}
		static int func2(int a, int b) {
			return a - b;
		}
		public static void Main(string[] args) {
			deal fun = new deal(func1);
			Console.WriteLine(fun(1, 2));
			Console.ReadKey(true);
		}
	}
}
