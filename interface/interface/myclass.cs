using System;

namespace Interface
{
	public interface MyInterface {
		//三个方法
		void Print();
		void Print2();
		void Print3();
	}
	
	public class BaseClass: MyInterface {
		public void Print() {
			Console.WriteLine ("Base Class");
		}
		//声明为虚函数，以让子类替换该方法
		public virtual void Print2() {
			Console.WriteLine ("Base Class");
		}
		//必须实现的接口
		public void Print3() {
			Console.WriteLine ("Print3");
		}
	}
	
	public class myclass : BaseClass
	{
		public myclass ()
		{
		}
		//隐藏基类的同名方法
		public void Print() {
			Console.WriteLine ("myclass");
		}
		//替换基类的同名方法
		public override void Print2() {
			Console.WriteLine ("myclass");
		}
		//基类继承一个接口，子类不是必须实现该接口的方法
	}
}

