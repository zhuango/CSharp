using System;

namespace Partial
{
	 public partial class A {
		partial void Print();
		public A(){}
		public void print() {
			Print ();
		}

	}
	
	public partial class A {
		//部分方法可有没有定义，执行该方法的地方会被删除
		//partial void Print() {
		//	Console.WriteLine("nima");
		//}
	}
	
	class MainClass
	{
		public static void Main (string[] args)
		{
			A a = new A();
			a.print();
			
			Console.WriteLine ("Hello World!");
		}
	}
}
