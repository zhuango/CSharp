using System;
using System.Collections;
namespace enumer
{
	public class Primes {
		private long min;
		private long max;
		
		public Primes() : this(2, 100)
		{
		}
		public Primes(long minimum, long maximum)
		{
			if (min < 2)
				min = 2;
			else
				min = minimum;
			max = maximum;
		}
		
		//something
		public IEnumerator GetEnumerator()
		{
			for (long possiblePrime = min; possiblePrime <= max; possiblePrime++)
			{
				bool isPrime = true;
				for (long possibleFactor = 2; possibleFactor <=
						(long)Math.Floor(Math.Sqrt(possiblePrime)); possibleFactor++)
				{
					long remainderAfterDivision = possiblePrime % possibleFactor;
					if (remainderAfterDivision == 0)
					{
						isPrime = false;
						break;
					}
				}
				if (isPrime)
				{
					//迭代器要迭代的东西
					yield return possiblePrime;
					//yield return 100l;
				}
			}
		}
	}
	//My own class for testing enumrator
	public class Book {
		private int[] data = new int[10];
		public Book() {
			for(int i = 0; i < 10; i++)
				data[i] = i;
		}
		//Test Enumrator
		public IEnumerator GetEnumerator() {
			for(int i = 0; i < 10; i++){
				yield return i;
			}
		}
	}
	class MainClass
	{
		//测试迭代一个类成员
		public static IEnumerable SimpleList() {
			yield return "string 1";
			yield return "string 2";
			Console.WriteLine ("In SimpleList");
			yield return "string 3";
			yield return "stirng 4";
			//yield break; 
			Console.WriteLine ("In simpleList, again");
		}
		//(1) 调用 collectionObject.GetEnumerator(),返回一个 IEnumerator 引用。这个方法可以通过
		//	IEnumerable 接口的实现代码来获得,但这是可选的。
		//(2) 调用所返回的 IEnumerator 接口的 MoveNext()方法。
		//(3) 如果 MoveNext()方法返回 true,就使用 IEnumerator 接口的 Current 属性获取对象的一个引
		//	用,用于 foreach 循环。
		//(4) 重复前面两步,直到 MoveNext()方法返回 false 为止,此时循环停止。
		
		
		//1.如果要迭代一个类,可使用方法 GetEnumerator(),其返回类型是 IEnumerator。
		//2.如果要迭代一个类成员,例如一个方法,则使用 IEnumerable。
		//Note: 在迭代器块中,使用 yield 关键字选择要在 foreach 循环中使用的值。

		public static void Main (string[] args)
		{
			//迭代一个类成员, 也会执行这个函数
			foreach(string item in SimpleList()) {
				Console.WriteLine (item);
			}
			//迭代一个类
			Primes primesFrom2To1000 = new Primes(2, 1000);
			foreach (long i in primesFrom2To1000)
				  Console.Write("{0} ", i);
			Console.WriteLine ();
			
			//My own testing
			Book b = new Book();
			foreach(int count in b)
				Console.Write(count + " ");
		}
	}
}