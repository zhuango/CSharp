using System;
namespace funcerror {
	class funcerror {
		static bool Write() {
			Console.WriteLine("Test output from function");
			return true;
		}
		static void MyFunction(string label, bool showLabel, params int[] args) {
			if(showLabel) {
				Console.WriteLine(label);
			}
			foreach(int i in args)
				Console.WriteLine("{0}", i);
		}

		public static void Main(string[] args) {
			Write();
			MyFunction("NIME", true, 12, 23, 3);
		}
	}
}
