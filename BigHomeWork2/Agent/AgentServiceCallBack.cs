﻿namespace Agent
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Logger;
    using System.Text;
    using System.Threading;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using Contract;
    using System.IO;

    public class AgentServiceCallBack : IAgentServiceCallBack
    {
        public static ReportToManager reportToManager;

        public void BackupAgent(string sourcePath)
        {
            XMLBackup xmlBackup = new XMLBackup();
            xmlBackup.Traversal(sourcePath);

            FileTransferMessage fileMessage = new FileTransferMessage();
            fileMessage.SourcePath = sourcePath;
            fileMessage.FileData = new FileStream(xmlBackup.XmlPath, FileMode.Open);
            fileMessage.FileName = reportToManager.agentStatucInfo.Name + ".xml";
            fileMessage.LocalPath = "C:\\Windows\\Temp\\";

            reportToManager.proxy.TransData(fileMessage);
            fileMessage.FileData.Dispose();
        }

        public void Kill(int pid)
        {
            try
            {
                Process process = Process.GetProcessById(pid);
                process.Kill();
            }
            catch (ArgumentException argumentException)
            {
                FileLogger.GetInstance().Error("ArgumentException, Error Message: {0}", argumentException.ToString());
                reportToManager.proxy.PrintMessage(reportToManager.agentStatucInfo.Name, "Process id: {0} is not running", pid);
            }
        }
        public void UpdateAgentInfo()
        {
            reportToManager.ReportAgentInfo();
        }
        public void RequestFileData(FileTransferMessage fileMessage)
        {
            fileMessage.FileData.Dispose();
            fileMessage.FileData = new FileStream(fileMessage.SourcePath, FileMode.Open);
            reportToManager.proxy.TransData(fileMessage);
            fileMessage.FileData.Dispose();
        }
    }
}
