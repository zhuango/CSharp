﻿namespace Agent
{
    using Logger;
    using System;
    using System.IO;
    using System.Xml;

    public class XMLBackup
    {
        private XmlDocument xmlDocument = null;
        private String xmlPath = null;
        private String sourcePath = String.Empty;
        //logger
        private ILogger logger = FileLogger.GetInstance();

        public String XmlPath { get { return xmlPath; } }

        public XMLBackup() :
            this(@"C:\Windows\Temp\FileInfo.xml")
        {
        }

        public XMLBackup(String xmlPath)
        {
            if (!Directory.Exists(xmlPath))
            {
                logger.Warning(":Source Directory \"{0}\" was not found, Use the {1}", xmlPath, @"C:\Windows\Temp\");
                xmlPath = @"C:\Windows\Temp\";
            }
            this.xmlPath = xmlPath + "FileInfo.xml";
        }
        public void Traversal(String path)
        {
            if (path.EndsWith("\\"))
            {
            }
            else
            {
                path = path + "\\";
            }
            if (!Directory.Exists(path))
            {
                logger.Error(":Directory {0} was not found", path);
                return;
            }
            this.sourcePath = path;
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            this.xmlDocument = new XmlDocument();

            this.traversal(path, this.xmlDocument);
            xmlDocument.Save(xmlPath);
        }

        private void traversal(String path, XmlNode RootxmlNode)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            var folderNode = this.GetXmlNode(directoryInfo, "Folder");
            RootxmlNode.AppendChild(folderNode);

            foreach (FileInfo fileInfo in directoryInfo.GetFiles())
            {
                try
                {
                    var fileNode = this.GetXmlNode(fileInfo, "File");
                    //Add size of file or directory
                    var sizeNode = xmlDocument.CreateAttribute("Size");
                    sizeNode.Value = fileInfo.Length.ToString();
                    fileNode.Attributes.Append(sizeNode);

                    folderNode.AppendChild(fileNode);
                }
                catch (DirectoryNotFoundException directoryNotFoundException)
                {
                    logger.Error(":Directory {0} was not found, Error Message:", path, directoryNotFoundException.ToString());
                }
            }
            foreach (DirectoryInfo directoryInfoTmp in directoryInfo.GetDirectories())
            {
                try
                {
                    this.traversal(directoryInfoTmp.FullName, folderNode);
                }
                catch (DirectoryNotFoundException directoryNotFoundException)
                {
                    logger.Error(":Directory {0} was not found, Error Message:", path, directoryNotFoundException.ToString());
                }
                catch (UnauthorizedAccessException unauthorizedAccessException)
                {
                    logger.Error(":Directory {0} was not found, Error Message:", path, unauthorizedAccessException.ToString());
                }
            }
        }

        private XmlNode GetXmlNode(FileSystemInfo fileSystemInfo, String elementName)
        {
            XmlElement node = xmlDocument.CreateElement(elementName);

            //Add CreateTime attribute
            node.Attributes.Append(this.GetXmlAttribute("CreateTime", fileSystemInfo.CreationTime.ToString()));
            //Add ModifyTime attribute
            node.Attributes.Append(this.GetXmlAttribute("ModifyTime", fileSystemInfo.LastWriteTime.ToString()));
            //Add full name attribute
            node.Attributes.Append(this.GetXmlAttribute("Path", fileSystemInfo.FullName));

            //Set Inner text of this node
            node.InnerText = fileSystemInfo.Name;

            return node;
        }
        private XmlAttribute GetXmlAttribute(String attributeName, String attributeValue)
        {
            XmlAttribute attribute = xmlDocument.CreateAttribute(attributeName);
            attribute.Value = attributeValue;
            return attribute;
        }
    }
}
