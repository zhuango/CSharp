﻿namespace Agent
{
    using Contract;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                NetTcpBinding binding = new NetTcpBinding();
                binding.Security.Mode = SecurityMode.None;
                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;
                EndpointAddress endPoint = new EndpointAddress("Net.tcp://192.168.1.105:6789");

                IAgentServiceCallBack agentServiceCallBack = new AgentServiceCallBack();

                InstanceContext instance = new InstanceContext(agentServiceCallBack);
                DuplexChannelFactory<IManagerService> channel = new DuplexChannelFactory<IManagerService>(instance, binding, endPoint);
                IManagerService proxy = channel.CreateChannel();

                ReportToManager reportToManager = new ReportToManager(proxy);
                reportToManager.Registration();
                AgentServiceCallBack.reportToManager = reportToManager;

                Console.ReadLine();
                reportToManager.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
