﻿namespace Agent
{
    using Contract;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ReportToManager : IDisposable
    {
        public IManagerService proxy;
        public AgentStatusInfo agentStatucInfo;
        System.Timers.Timer timer;

        public ReportToManager(IManagerService proxy)
        {
            this.proxy = proxy;
            this.agentStatucInfo = new AgentStatusInfo();
        }

        public void Registration()
        {
            this.InitAgentStatusInfo();
            proxy.Registration(agentStatucInfo);
            timer = new System.Timers.Timer();
            timer.Elapsed += new System.Timers.ElapsedEventHandler((sender, ex) => { this.ReportAgentInfo(); });
            timer.Enabled = true;
            /* Every 5 second repotion */
            timer.Interval = 5000;
            timer.Start();
        }

        private void InitAgentStatusInfo()
        {
            Console.WriteLine("What is your name:");
            String name = Console.ReadLine();

            while (proxy.IsNameExist(name))
            {
                Console.WriteLine("Name have existed in server, What is your name, again:");
                name = Console.ReadLine();
            }
            agentStatucInfo.Name = name;
            this.UpdateAgentInfo();
        }

        public void ReportAgentInfo()
        {
            this.UpdateAgentInfo();
            proxy.ReportAgentStatusInfo(this.agentStatucInfo);
        }

        public void UpdateAgentInfo()
        {
            agentStatucInfo.ProcessInfoList.Clear();
            foreach (Process process in System.Diagnostics.Process.GetProcesses())
            {
                agentStatucInfo.ProcessInfoList.Add(new ProcessInfo(process.ProcessName, process.Id, process.SessionId, process.WorkingSet64));
            }
        }

        public void Dispose()
        {
            proxy.RemoveAgent(agentStatucInfo.Name);
            timer.Dispose();
            (proxy as IDisposable).Dispose();
        }
    }
}
