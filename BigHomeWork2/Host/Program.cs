﻿namespace Host
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using Manager;
    using System.IO;
    using System.Net;

    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (ServiceHost host = new ServiceHost(typeof(ManagerService)))
                {
                    host.Opened += host_Opened;
                    host.Open();

                    Console.WriteLine("type \"help\" for usage.");
                    while (true)
                    {
                        Console.Write("[{0}@{1}]$ ", System.Environment.UserName, Dns.GetHostName());
                        String commandLine = Console.ReadLine();
                        
                        CommandAnalysis comAnalysis = new CommandAnalysis(commandLine, "\\s+");
                        String[] commands = comAnalysis.GetParamerByChar(' ');
                        List<String> paramreList = new List<string>(commands);
                        if (commands.Length == 1 && commands[0].Equals("exit"))
                        {
                            break;
                        }
                        if (commands.Length > 4)
                        {
                            Console.WriteLine("ERROR: paramer number is invalid");
                            continue;
                        }
                        CommandExecution comExecution = new CommandExecution(paramreList, ManagerService.agentManagerDictionary);
                        comExecution.Execute();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
        }
        static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("Server Ready");
        }
    }
}
