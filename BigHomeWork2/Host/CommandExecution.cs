﻿namespace Host
{
    using Contract;
    using Manager;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class CommandExecution
    {
        private List<String> commandParamers;
        private Dictionary<String, AgentManager> agentManagerDictionary;
        private delegate void operation();
        private Dictionary<String, operation> operationSet;

        public CommandExecution(List<String> paramers, Dictionary<String, AgentManager> agentManagerDictionary)
        {
            this.agentManagerDictionary = agentManagerDictionary;
            this.commandParamers = paramers;
            this.InitOperations();
        }

        private void InitOperations()
        {
            if (operationSet == null)
            {
                operationSet = new Dictionary<string, operation>();
                operationSet.Add("help", Help);
                operationSet.Add("view", View);
                operationSet.Add("status", Status);
                operationSet.Add("remove", Remove);
                operationSet.Add("kill", Kill);
                operationSet.Add("backup", Backup);
            }
        }
        public void Execute()
        {
            if (operationSet.Keys.Contains(commandParamers[0]))
            {
                operationSet[commandParamers[0]]();
            }
            else
            {
                Console.WriteLine("ERROR: Invalid agrument/option");
            }
        }

        private Boolean HasNParamer(int n)
        {
            if (commandParamers.Count == n)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Boolean HasThisAgent(String agentName)
        {
            if (agentManagerDictionary.Keys.Contains(agentName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void Help()
        {
            using (FileStream fileStream = new FileStream(@"../../OperationExplanation.txt", FileMode.Open))
            {
                StreamReader streamReader = new StreamReader(fileStream);
                while (!streamReader.EndOfStream)
                {
                    Console.WriteLine(streamReader.ReadLine());
                }
            }
        }
        private void View()
        {
            foreach (KeyValuePair<String, AgentManager> pair in agentManagerDictionary)
            {
                Console.WriteLine(pair.Key);
            }
        }
        private void Status()
        {
            if (this.HasNParamer(2))
            {
                if (HasThisAgent(commandParamers[1]))
                {
                    Console.WriteLine(agentManagerDictionary[commandParamers[1]].GetAgentStatusInfoMessage());
                }
                else
                {
                    Console.WriteLine("ERROR: There is no this agent:{0}", commandParamers[1]);
                }
            }
        }

        private void Remove()
        {
            if (this.HasNParamer(2))
            {
                if (this.HasThisAgent(commandParamers[1]))
                {
                    agentManagerDictionary.Remove(commandParamers[1]);
                }
                else
                {
                    Console.WriteLine("Agent : {0} have been removed or does not exist", commandParamers[0]);
                }
            }
        }

        private void Kill()
        {
            if (this.HasNParamer(3))
            {
                if (this.HasThisAgent(commandParamers[2]))
                {
                    Int32 pid;
                    if (Int32.TryParse(commandParamers[1], out pid))
                    {
                        agentManagerDictionary[commandParamers[2]].Kill(pid);
                    }
                    else
                    {
                        Console.WriteLine("Invalid pid :{0}", commandParamers[1]);
                    }
                }
            }
        }

        private void Backup()
        {
            if (this.HasNParamer(4) && this.HasThisAgent(commandParamers[1]))
            {
                this.agentManagerDictionary[commandParamers[1]].Backup(commandParamers[2]);
                String xmlPath = String.Format("C:\\Windows\\Temp\\");
                Thread.Sleep(1000);
                XMLRestore xmlRestore = new XMLRestore(xmlPath, this.agentManagerDictionary[commandParamers[1]]);
                xmlRestore.Restore(commandParamers[3]);
            }
        }
    }
}
