﻿namespace Manager
{
    using Contract;
    using Logger;
    using System;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Xml;

    public class XMLRestore
    {
        private AgentManager agentManager;
        private XmlDocument sourceXmlDocument;
        private String sourceXmlPath;
        private String destinPath = String.Empty;
        private ILogger logger = FileLogger.GetInstance();
        private DriveInfo driverInfo;
        public String DestinPath
        {
            get
            {
                return this.destinPath;
            }
        }

        public XMLRestore(String xmlFilePath, AgentManager agentManager)
        {
            if (!Directory.Exists(xmlFilePath))
            {
                logger.Error(":Source Directory \"{0}\" was not found! Restore failed!", xmlFilePath);
                return;
            }
            this.agentManager = agentManager;
            this.sourceXmlPath = String.Format("{0}{1}.xml", xmlFilePath, agentManager.AgentStatusInfo.Name);
        }

        public void Restore(String destinPath)
        {
            if (destinPath.EndsWith("\\"))
            {
            }
            else
            {
                destinPath = destinPath + "\\";
            }
            this.destinPath = destinPath;
            this.driverInfo = new DriveInfo(destinPath.Substring(0, 1));
            try
            {
                //Be sure the root directory is created
                DirectoryInfo directoryInfo = Directory.CreateDirectory(destinPath);
                logger.Info(":Directory: {0} was Created", destinPath);
                if (sourceXmlDocument == null)
                {
                    sourceXmlDocument = new XmlDocument();
                    try
                    {
                        sourceXmlDocument.Load(sourceXmlPath);
                    }
                    catch (FileNotFoundException fileNotFoundException)
                    {
                        logger.Error(":Backup xml File {0} was not found or not a xml file, Error Message, {1}", sourceXmlPath, fileNotFoundException.ToString());
                        return;
                    }
                }
                restore(destinPath, sourceXmlDocument);
                //this.SetFileInfo(directoryInfo, xmlDocument);
            }
            catch (IOException IOException)
            {
                logger.Error(":Directory: {0}, Error Message: {1}", destinPath, IOException.ToString());
                return;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                logger.Error(":Directory: {0} {1}", destinPath, unauthorizedAccessException.ToString());
                return;
            }
            catch (Exception e)
            {
                logger.Error(":Directory: {0} {1}", destinPath, e.ToString());
                return;
            }
        }
        private void restore(String path, XmlNode sourceXmlNodeArg)
        {
            foreach (XmlNode xmlNode in sourceXmlNodeArg.ChildNodes)
            {
                //Console.WriteLine(xmlNode.Name);
                if (xmlNode.Name.Equals("File"))
                {
                    this.RestoreFile(path, xmlNode);
                }
                else if (xmlNode.Name.Equals("Folder"))
                {
                    String directoryFullName = String.Format("{0}{1}\\", path, xmlNode.FirstChild.Value);
                    try
                    {
                        DirectoryInfo directoryInfo = Directory.CreateDirectory(directoryFullName);
                        logger.Info(":Directory: {0} was Created", directoryFullName);
                        restore(directoryFullName, xmlNode);
                        this.SetFileInfo(directoryInfo, xmlNode);
                    }
                    catch (IOException IOException)
                    {
                        logger.Error(":Directory: {0} IOException, Error Message: {1}", directoryFullName, IOException.ToString());
                    }
                    catch (UnauthorizedAccessException unauthorizedAccessException)
                    {
                        logger.Error(":Directory: {0} Unauthorized Access Exception, Error Message: {1}", directoryFullName, unauthorizedAccessException.ToString());
                    }
                    catch (Exception e)
                    {
                        logger.Error(":Directory: {0} was not found 3, Error Message: {1}", directoryFullName, e.ToString());
                    }
                }
            }
        }

        private void RestoreFile(String sourceFilePath, XmlNode xmlNode)
        {
            String fileFullName = String.Format("{0}{1}", sourceFilePath, xmlNode.InnerText);
            //Be sure available free space is big enough
            if (int.Parse(xmlNode.Attributes["Size"].Value) > this.driverInfo.AvailableFreeSpace)
            {
                logger.Error(":File {0} can not be created, Error Message: Insufficient Disk Space, Drive \"{1}\" AvailableFreeSpace {2}", fileFullName, this.driverInfo.Name, this.driverInfo.AvailableFreeSpace);
                return;
            }
            try
            {
                FileInfo sourceFileInfo = new FileInfo(fileFullName);
                Int32 sourceFilesize;
                FileTransferMessage fileMessage = new FileTransferMessage();

                if (!Int32.TryParse(xmlNode.Attributes["Size"].Value, out sourceFilesize))
                {
                    sourceFilesize = 0;
                }
                if (sourceFileInfo.Exists)
                {
                    if (sourceFileInfo.Length == sourceFilesize)
                    {
                        logger.Info("File: {0} is already backup", fileFullName);
                        return;
                    }
                    else
                    {
                        fileMessage.FileOffset = sourceFilesize;
                    }
                }

                fileMessage.FileName = xmlNode.InnerText;
                fileMessage.LocalPath = sourceFilePath;
                fileMessage.SourcePath = xmlNode.Attributes["Path"].Value.ToString();
                fileMessage.FileData = new FileStream(fileFullName, FileMode.OpenOrCreate);

                agentManager.RequestFile(fileMessage);
                fileMessage.FileData.Dispose();

                FileInfo fileInfo = new FileInfo(fileFullName);
                this.SetFileInfo(fileInfo, xmlNode);
                logger.Info(":File {0} was created", fileFullName);
            }
            catch (IOException IOException)
            {
                logger.Error(":File {0} was not found ErrorMessge: {1}", fileFullName, IOException.ToString());
            }
        }
        private void SetFileInfo(FileSystemInfo fileSystemInfo, XmlNode xmlNode)
        {
            try
            {
                fileSystemInfo.CreationTime = Convert.ToDateTime(xmlNode.Attributes["CreateTime"].Value);
                fileSystemInfo.LastWriteTime = Convert.ToDateTime(xmlNode.Attributes["ModifyTime"].Value);
            }
            catch (Exception ex)
            {
                logger.Error("Time Set failed, Error Message: {0}", ex.ToString());
            }
        }
    }
}
