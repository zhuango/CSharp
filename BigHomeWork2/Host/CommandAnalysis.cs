﻿namespace Host
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    class CommandAnalysis
    {
        private String oldString;
        Regex regex;

        public CommandAnalysis(String old, String pattern)
        {
            this.oldString = old.TrimEnd(' ').TrimStart(' ');
            this.regex = new Regex(pattern);
        }

        public String[] GetParamerByChar(char spliter)
        {
            String newString = regex.Replace(oldString, " ");
            return newString.Split(spliter);
        }
    }
}
