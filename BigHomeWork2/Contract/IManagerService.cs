﻿

namespace Contract
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;
    using System.Threading.Tasks;
    [ServiceContract(Namespace = "http://www.AvePoint.com", CallbackContract =typeof(IAgentServiceCallBack))]
    public interface IManagerService
    {
        [OperationContract]
        Boolean IsNameExist(String name);

        [OperationContract(IsOneWay = true)]
        void PrintMessage(String AgentName, String messageFormat, params object[] parameters);

        [OperationContract(IsOneWay = true)]
        void Registration(AgentStatusInfo agentStatusInfo);

        [OperationContract(IsOneWay = true)]
        void ReportAgentStatusInfo(AgentStatusInfo agentStatusInfo);

        [OperationContract(IsOneWay = true)]
        void RemoveAgent(String AgentName);

        [OperationContract(IsOneWay = true)]
        void TransData(FileTransferMessage sourceFileStream);
    }
}