﻿namespace Contract
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    //[DataContract(Namespace = "http://www.AvePoint.com")]
    //public enum SessionType
    //{
    //    [EnumMember]
    //    Services = 0,

    //    [EnumMember]
    //    Console = 1
    //}

    [DataContract(Namespace = "http://www.AvePoint.com")]
    public class AgentStatusInfo
    {
        public AgentStatusInfo()
        {
            ProcessInfoList = new List<ProcessInfo>();
        }
        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String CPUUsage { get; set; }

        [DataMember]
        public String MemoryUsage { get; set; }

        [DataMember]
        public List<ProcessInfo> ProcessInfoList { get; set; }
    }

    [DataContract(Namespace = "http://www.AvePoint.com")]
    public class ProcessInfo
    {
        public ProcessInfo(String processName, int id, int sessionId, long workingSet64)
        {
            this.ProcessName = processName;
            this.Id = id;
            this.SessionId = sessionId;
            this.WorkingSet64 = workingSet64;
        }
        [DataMember]
        public String ProcessName { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int SessionId { get; set; }

        [DataMember]
        public long WorkingSet64 { get; set; }
    }


}
