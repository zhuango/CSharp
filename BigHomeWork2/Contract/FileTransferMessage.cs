﻿namespace Contract
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;
    using System.Threading.Tasks;

    [MessageContract]
    public class FileTransferMessage
    {
        [MessageHeader(MustUnderstand = true)]
        public long FileOffset;

        [MessageHeader(MustUnderstand = true)]
        public string SourcePath;//文件保存路径

        [MessageHeader(MustUnderstand = true)]
        public string LocalPath;//文件保存路径

        [MessageHeader(MustUnderstand = true)]
        public string FileName;//文件名称

        [MessageBodyMember(Order = 1)]
        public Stream FileData;//文件传输时间
    }
}
