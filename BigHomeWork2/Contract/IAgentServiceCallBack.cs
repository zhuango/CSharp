﻿namespace Contract
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.Text;
    using System.Threading.Tasks;

    [ServiceContract(Namespace="http://www.AvePoint.com")]
    public interface IAgentServiceCallBack
    {
        [OperationContract(IsOneWay = true)]
        void Kill(int pid);

        [OperationContract(IsOneWay = true)]
        void BackupAgent(String fileMessage);

        [OperationContract(IsOneWay = true)]
        void UpdateAgentInfo();

        [OperationContract(IsOneWay = true)]
        void RequestFileData(FileTransferMessage fileMessage);
    }
}
