﻿namespace Manager
{
    using Contract;
    using System;
    using System.IO;
    using System.Text;

    public class AgentManager
    {
        private AgentStatusInfo agentStatusInfo;
        private IAgentServiceCallBack agentServiceCallBack;

        public AgentStatusInfo AgentStatusInfo
        {
            get { return agentStatusInfo; }
            set { agentStatusInfo = value; }
        }

        public AgentManager(AgentStatusInfo agentStatusInfo, IAgentServiceCallBack agentServiceCallBack)
        {
            this.agentStatusInfo = agentStatusInfo;
            this.agentServiceCallBack = agentServiceCallBack;
        }

        public void Kill(int pid)
        {
            agentServiceCallBack.Kill(pid);
        }

        public String GetAgentStatusInfoMessage()
        {
            agentServiceCallBack.UpdateAgentInfo();
            StringBuilder stringBuilder = new StringBuilder();
            //stringBuilder.Append(String.Format("{0,-25}{1, 8} {2, -16}{3, 11}{4, 12}\n", "Image Name", "PID", "Session Name", "Session#", "Mem Usage"));
            stringBuilder.Append(String.Format("{0,-25}{1, 8} {2, -16} {3, 12}\n", "Image Name", "PID", "Session#", "Mem Usage"));
            stringBuilder.Append(String.Format("==============================================================================\n"));

            foreach (ProcessInfo process in agentStatusInfo.ProcessInfoList)
            {
                //stringBuilder.Append(String.Format("{0,-25}{1, 8} {2, -16}{3, 11}{4, 12} K\n", process.ProcessName, process.Id, process.SessionId, (int)process.SessionId, process.WorkingSet64 / 1024));
                stringBuilder.Append(String.Format("{0,-25}{1, 8} {2, -16} {3, 12} K\n", process.ProcessName, process.Id, process.SessionId, process.WorkingSet64 / 1024));
            }
            return stringBuilder.ToString();
        }

        public void Backup(String sourcePath)
        {
            agentServiceCallBack.BackupAgent(sourcePath);
        }

        public void RequestFile(FileTransferMessage fileMessage)
        {
            agentServiceCallBack.RequestFileData(fileMessage);
        }
    }
}
