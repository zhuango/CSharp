﻿namespace Manager
{
    using Contract;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using System.Net;

    public class ManagerService : IManagerService
    {
        public static Dictionary<String, AgentManager> agentManagerDictionary = new Dictionary<String, AgentManager>();

        public ManagerService()
        {
        }
        public void Registration(AgentStatusInfo agentStatusInfo)
        {
            /*Get Call back*/
            IAgentServiceCallBack agentCallBack = OperationContext.Current.GetCallbackChannel<IAgentServiceCallBack>();
            agentManagerDictionary.Add(agentStatusInfo.Name, new AgentManager(agentStatusInfo, agentCallBack));
        }

        public void RemoveAgent(String agentName)
        {
            agentManagerDictionary.Remove(agentName);
        }

        public void ReportAgentStatusInfo(AgentStatusInfo agentStatusInfo)
        {
            String agentName = agentStatusInfo.Name;
            agentManagerDictionary[agentName].AgentStatusInfo = agentStatusInfo;
        }

        public void TransData(FileTransferMessage sourceFileStream)
        {
            String Name = String.Format("{0}{1}", sourceFileStream.LocalPath, sourceFileStream.FileName);
            FileStream fileStream = new FileStream(Name, FileMode.OpenOrCreate);

            if (sourceFileStream.FileData.CanSeek && fileStream.CanSeek)
            {
                sourceFileStream.FileData.Seek(sourceFileStream.FileOffset, SeekOrigin.Begin);
                fileStream.Seek(sourceFileStream.FileOffset, SeekOrigin.Begin);
            }
            StreamReader fileReader = new StreamReader(sourceFileStream.FileData);

            while (!fileReader.EndOfStream)
            {
                String line = fileReader.ReadLine();
                Byte[] buffer = Encoding.UTF8.GetBytes(line);
                fileStream.Write(buffer, 0, buffer.Length);
            }
            fileStream.Dispose();
            fileReader.Dispose();
            sourceFileStream.FileData.Dispose();
        }

        public void PrintMessage(String agentName, string messageFormat, params object[] parameters)
        {
            StringBuilder message = new StringBuilder();
            message.AppendFormat("\n[{0}] report: ", agentName);
            message.AppendFormat(messageFormat, parameters);
            Console.WriteLine(message);
            Console.Write("\n[{0}@{1}]$ ", System.Environment.UserName, Dns.GetHostName());
        }
        public Boolean IsNameExist(string name)
        {
            if (agentManagerDictionary.Keys.Contains(name))
            {
                return true;
            }
            return false;
        }
    }
}
