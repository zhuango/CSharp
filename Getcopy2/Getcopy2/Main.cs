using System;

namespace Getcopy2
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Cloner c = new Cloner(100);
			Cloner d = (Cloner)c.Getcopy();
			Console.WriteLine (c.GetContentData());
			Console.WriteLine (d.GetContentData());
			
			c.SetContentData(102);
			Console.WriteLine(c.GetContentData ());
			Console.WriteLine(d.GetContentData ());
			
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			Cloner e = (Cloner)d.Clone();
			c.SetContentData(1002);
			e.SetContentData(10000);
			Console.WriteLine(c.GetContentData ());
			Console.WriteLine(d.GetContentData ());
			Console.WriteLine(e.GetContentData ());
			Console.WriteLine ("Hello World!");
		}
	}
}
