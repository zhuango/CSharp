using System;

namespace Getcopy2
{
	public class Content {
		public int data = 10;
		public Content()
		{
			
		}
	}
	public class Cloner : ICloneable
	{
		public Content c = new Content();
		public Cloner (int data)
		{
			c.data = data;
		}
		public void SetContentData(int a) {
			c.data = a;
		}
		public int GetContentData() {
			return c.data;
		}
		//浅复制
		public object Getcopy() {
			return MemberwiseClone();
		}
		//深复制
		public object Clone() {
			Cloner newc = new Cloner(GetContentData());
			return newc;
		}

	}
}

