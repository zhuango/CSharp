﻿namespace HomeWorkTicket
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Threading;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Assembly assembly = Assembly.LoadFrom("TicketInfo.dll");
                //Create a new instance of TicketBox.TicketInfo
                Object obj = assembly.CreateInstance("TicketBox.TicketInfo");
                BindingFlags flags = (BindingFlags.NonPublic | BindingFlags.Public |
                    BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                Type type = obj.GetType();
                //Get the field named ticketList
                FieldInfo fieldInfo = type.GetField("ticketList", flags);
                List<String> listString = (List<String>)fieldInfo.GetValue(obj);

                const Int32 windowsCount = 10;
                ManualResetEvent[] doneEvents = new ManualResetEvent[windowsCount];
                for (int i = 0; i < 10; i++)
                {
                    doneEvents[i] = new ManualResetEvent(false);
                    TicketSeller ticketSeller = new TicketSeller(listString, doneEvents[i]);
                    ThreadPool.QueueUserWorkItem(ticketSeller.TicketSell, i);
                }
                WaitHandle.WaitAll(doneEvents);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
