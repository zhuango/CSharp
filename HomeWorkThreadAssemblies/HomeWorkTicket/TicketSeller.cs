﻿namespace HomeWorkTicket
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    class TicketSeller
    {
        private List<String> ticketBox = null;
        private static int count;
        private static Object objForLock;
        private ManualResetEvent manualResetEvent = null;

        public TicketSeller(List<String> ticketBox, ManualResetEvent doneEvent)
        {
            this.ticketBox = ticketBox;
            TicketSeller.count = 0;
            objForLock = new Object();
            manualResetEvent = doneEvent;
        }

        public void TicketSell(Object threadContext)
        {
            while (count < ticketBox.Count)
            {
                lock (objForLock)
                {
                    if (count < ticketBox.Count)
                    {
                        //Processing ...
                        Thread.Sleep(500);
                        Console.WriteLine("Windows {0} selled {1} At {2}!", (int) threadContext, ticketBox[count], DateTime.Now.ToString());
                        TicketSeller.count++;
                    }
                }
            }
            Console.WriteLine("Windows {0} done cause All tickets were selled!", (int)threadContext);
            manualResetEvent.Set();
        }
    }
}
