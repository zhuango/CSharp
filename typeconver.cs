using System;
namespace TypeConvert {
	class typeConvert {
		public static void Main(string[] args) {
			int a = 10;
			short b = 12;
			bool c = true;
			byte d = 8;
			float e = 12.2f;

			Console.WriteLine(a);
			a = b;
			Console.WriteLine(a);
			//int --> short, error
			//b = a;
			//Console.WriteLine(b);
			
			//bool --> stirng , error
			//string cs = c;
			
			//byte --> float
			e = d;
			Console.WriteLine(e);
		}
	}
}
