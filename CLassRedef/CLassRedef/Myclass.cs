using System;

namespace CLassRedef
{
	public class Myclass
	{
		public class A{
			public A(){}
			public void Print() {
				Console.WriteLine("This is A");
			}
		}
		public Myclass ()
		{
		}
		private Myclass.A a = new Myclass.A();
		public void Print() {
			a.Print();
			Console.WriteLine ("This is Myclass");
		}
	}
}

