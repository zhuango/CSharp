﻿namespace HomeworkBCL
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MyStack<int> stackInt = new MyStack<int>();
                Int32 a = stackInt.Pop();
                Console.WriteLine("Empty Pop: {0}", a);

                Console.WriteLine("*****************  Push ******************************");
                for (int i = 0; i < 10; i++)
                {
                    stackInt.Push(i);
                }
                //Throw a StackOverflowException, assuming stack size is 10
                //stackInt.Push(8);
                Console.WriteLine(stackInt.ToString());

                Console.WriteLine("*****************  Pop  ******************************");
                Console.WriteLine("Pop: {0}", stackInt.Pop().ToString());
                Console.WriteLine("Pop: {0}", stackInt.Pop().ToString());
                Console.WriteLine("Pop: {0}", stackInt.Pop().ToString());
                Console.WriteLine(stackInt.ToString());

                Console.WriteLine("*****************foreach******************************");
                foreach (int tmp in stackInt)
                {
                    Console.WriteLine(tmp);
                    //throw exception if the stack in foreach was Changed, 
                    //stackInt.Push(10000);
                }

                Console.WriteLine("****************** Tree  *****************************");
                TreeNode root = GetTree();
                Console.WriteLine(root.ToString());
                Console.WriteLine("******************  DFS  *****************************");
                DFS(root);
            }
            catch (StackOverflowException e)
            {
                Console.WriteLine(e.ToString());
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        static void DFS(TreeNode root)
        {
            MyStack<TreeNode> stack = new MyStack<TreeNode>();
            TreeNode treeNode = null;
            stack.Push(root);

            while (!stack.IsEmpty())
            {
                treeNode = stack.Pop();
                Console.WriteLine("{0}({1})", treeNode.Name, treeNode.Value);
                //Add all children to stack, from right to left
                Int32 count = treeNode.ChildrenCount - 1;
                while (count >= 0)
                {
                    stack.Push(treeNode.GetChild(count));
                    count--;
                }
            }
        }
        static TreeNode GetTree()
        {
            TreeNode root = GetTreeNode(5, "A");
            TreeNode nodeB = GetTreeNode(3, "B");
            TreeNode nodeC = GetTreeNode(10, "C");
            TreeNode nodeD = GetTreeNode(5, "D");
            TreeNode nodeE = GetTreeNode(6, "E");
            TreeNode nodeF = GetTreeNode(0, "F");
            TreeNode nodeA = GetTreeNode(3, "A");

            root.AddChild(nodeB);
            root.AddChild(nodeC);

            nodeB.AddChild(nodeD);
            nodeB.AddChild(nodeE);

            nodeC.AddChild(nodeF);
            nodeC.AddChild(nodeA);

            return root;
        }
        static TreeNode GetTreeNode(Int32 value, string name)
        {
            TreeNode newNode = new TreeNode();
            newNode.Value = value;
            newNode.Name = name;

            return newNode;
        }
    }
}
