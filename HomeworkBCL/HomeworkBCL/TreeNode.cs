﻿namespace HomeworkBCL
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    public class TreeNode : IComparable<TreeNode>
    {
        public string Name { get; set; }
        public int Value { get; set; }

        /// <summary>
        /// Enumerator中使用
        /// </summary>
        private int version = 0;

        private List<TreeNode> children;

        private TreeNode Parent;

        public TreeNode()
        {
            this.Name = string.Empty;
            this.children = new List<TreeNode>();
        }

        #region Basic Method
        public void AddChild(TreeNode node)
        {
            CheckArgumentNull(node, "node");
            if (this.children.Contains(node))
            {
                throw new ArgumentException();
            }
            this.children.Add(node);
            node.Parent = this;
            AddVersion();
        }

        public bool RemoveChild(string name)
        {
            TreeNode node = this.GetChild(name);
            node.Parent = null;
            AddVersion();
            return this.children.Remove(node);
        }
        public TreeNode GetChild(Int32 index)
        {
            if (index < 0 || index > this.ChildrenCount - 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            return this.children[index];
        }

        public TreeNode GetChild(string name)
        {
            CheckArgumentNull(name, "name");
            foreach (TreeNode node in this.children)
            {
                if (string.Equals(node.Name, name, StringComparison.Ordinal))
                {
                    return node;
                }
            }
            return null;
            //return this.children.FirstOrDefault(node => string.Equals(node.Name, name, StringComparison.Ordinal));//可以用Foreach等价实现
        }

        public int ChildrenCount { get { return this.children.Count; } }

        private void CheckArgumentNull<T>(T arg, string argName)
            where T : class
        {
            if (arg == null)
            {
                throw new ArgumentNullException(argName);
            }
        }

        public void SortChildrenByName()
        {
            this.children.Sort();
            AddVersion();
        }

        private void AddVersion()
        {
            TreeNode current = this;
            do
            {
                current.version++;
                current = current.Parent;
            }
            while (current != null);
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            AppendChild(this, string.Empty, builder);
            return builder.ToString();
        }

        private void AppendChild(TreeNode node, string deepth, StringBuilder builder)
        {
            string name = string.Empty;
            int value = 0;
            if (node != null)
            {
                name = node.Name;
                value = node.Value;
            }
            builder.AppendFormat("{0}{1}({2})", deepth, name, value);
            builder.AppendLine();
            foreach (TreeNode child in node.children)
            {
                AppendChild(child, deepth + "  ", builder);
            }
        }
        #endregion

        #region Equals and GetHasCode
        /// <summary>
        /// 为了List.Contains方法
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var node = obj as TreeNode;
            if (node == null)
            {
                return false;
            }
            return string.Equals(this.Name, node.Name, StringComparison.Ordinal);
        }

        /// <summary>
        /// 重写Equals必须重新GetHashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        #endregion

        /// <summary>
        /// 实现IComparable<TreeNode>接口,用于排序
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(TreeNode other)
        {
            if (other == null)
            {
                return -1;
            }
            return string.Compare(this.Name, other.Name, StringComparison.Ordinal);
        }

    }
}
