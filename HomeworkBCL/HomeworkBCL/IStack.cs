﻿namespace HomeworkBCL
{
    using System;
    interface IStack<T>
    {
        void Push(T t);
        T Pop();
        T Peek();
        void Empty();
        Int32 Count();
    }
}
