using System;

namespace OperatorOverride
{
	class MainClass
	{
		public class A {
			public int data;
			
			public static A operator +(A add1, A add2) {
				A a = new A();
				a.data = add1.data + add2.data;
				return a;
			}
		}
		
		public static void Main (string[] args)
		{
			A a = new A();
			a.data = 100;
			
			A b = new A();
			b.data = 10;
			
			a = a + b;
			
			Console.WriteLine (a.data);
			Console.WriteLine (b.data);
			Console.WriteLine ("Hello World!");
		}
	}
}
