﻿namespace HomeWorkADO
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    //using System.Collections;
    using System.Collections.Generic;
    //using System.Linq;
    using System.Text;
    public class Company
    {
        private String connString = null;
        private String cmdAddDepartment = "INSERT INTO [dbo].[Department] ([Name] ,[Introduction]) VALUES (@name ,@Introduction)";
        private String cmdAddEmplyee = "INSERT INTO [dbo].[Emplyees] ([Id], [Name] ,[Gender], [Department], [Title]) VALUES (@Id, @Name ,@Gender, @Department, @Title)";
        private String cmdAddManegement = "INSERT INTO [dbo].[Management] ([Id] ,[Department]) VALUES (@Id ,@Department)";
        private String cmdDelectManagementByDepartment = "DELETE FROM [dbo].[Management]  WHERE [Department] = @Department";
        private String cmdDeleteEmplyeeById = "DELETE FROM [dbo].[Emplyees] WHERE [Id] = @Id";
        private String cmdQueryApartment = "SELECT [Name] ,[Introduction] FROM [ADO].[dbo].[Department]";
        private String cmdSelectMinisterIdByDepartment = "SELECT [Id] FROM [dbo].[Management] WHERE [Department] = @DepartmentName";
        //private String cmdSelectEmplyeeNameById = "SELECT [Name] FROM [ADO].[dbo].[Emplyees] WHERE [Id] = @Id";
        private String cmdSelectEmplyeeById = "SELECT [Id]  ,[Name] ,[Gender]  ,[Department] ,[Title] FROM [ADO].[dbo].[Emplyees] WHERE [Id] = @Id";
        private String cmdSelectManagementById = "SELECT [Id], [Department] FROM [dbo].[Management]  WHERE [Id] = @Id";
        private String cmdSelectDepartmentByName = "SELECT [Introduction] FROM [dbo].[Department] where [Name] = @Name";
        private String cmdSelectIdByDepartment = "SELECT [Id] FROM [ADO].[dbo].[Emplyees] WHERE [Department] = @Department";
        private String cmdSelectEmplyeeByName = "SELECT [Id]  ,[Name] ,[Gender]  ,[Department] ,[Title] FROM [ADO].[dbo].[Emplyees] WHERE [Name] = @Name";
        private String cmdUpdateManagement = "UPDATE [dbo].[Management] SET [Id] = @Id WHERE [Department] = @Department";
        private String cmdUpdateEmplyee = "UPDATE [dbo].[Emplyees] SET [Name] = @Name  ,[Gender] = @Gender ,[Department] = @Department,[Title] = @Title WHERE [Id] = @Id";
        public Company()
        {
            connString = ConfigurationManager.ConnectionStrings["ado"].ConnectionString;
        }

        public void AddDepartment(String name, String introduction)
        {
            using (SqlConnection conn = new SqlConnection(this.connString))
            {
                SqlParameter namePara = new SqlParameter("@name", name);
                SqlParameter intrPara = new SqlParameter("@Introduction", introduction);

                SqlCommand cmd = new SqlCommand(cmdAddDepartment, conn);
                conn.Open();
                cmd.Parameters.Add(namePara);
                cmd.Parameters.Add(intrPara);
                Int32 effectLine = cmd.ExecuteNonQuery();
                if (effectLine == 1)
                {
                    Console.WriteLine("{0} {1} add successfully", name, introduction);
                }
                else
                {
                    Console.WriteLine("Something Wrong!");
                }
            }
        }
        public Boolean NoThisDepartment(List<String> departments)
        {
            foreach (String department in departments)
            {
                if (this.GetDepartmentByName(department) == null)
                {
                    Console.WriteLine("Department {0} do not exist", department);
                    return true;
                }
            }
            return false;
        }
        public Emplyee FindEmplyeeByName(String Name)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdSelectEmplyeeByName, conn);
            SqlParameter namePara = new SqlParameter("@Name", Name);

            conn.Open();
            cmd.Parameters.Add(namePara);
            SqlDataReader reader = cmd.ExecuteReader();

            Emplyee emplyee = null;
            while (reader.Read())
            {
                emplyee = new Emplyee();
                Int32 idInt;
                try
                {
                    idInt = Int32.Parse(reader["Id"].ToString());
                    emplyee = this.FindEmplyeeById(idInt);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Int32.Parse(Strnig) Error: {0}", ex.ToString());
                }
            }
            return emplyee;
        }
        public void AddEmplyee(Emplyee emplyee)
        {
            if (this.NoThisDepartment(emplyee.Departments))
            {
                return;
            }
            if (this.FindEmplyeeById(emplyee.Id) != null)
            {
                Console.WriteLine("Your Id \"{0}\" is in use", emplyee.Id);
                return;
            }
            if (this.FindEmplyeeByName(emplyee.Name) != null)
            {
                Console.WriteLine("A exited emplyee has same name of your");
                try
                {
                    emplyee.Name = String.Format("{0}{1}", emplyee.Name, emplyee.Id.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Stirng.Format() Error: {0}", ex.ToString());
                }
            }
            this.AddEmplyee(emplyee.Id, emplyee.Name, emplyee.Gender, emplyee.GetDepartmentsString(), emplyee.Title);
            if (emplyee.Title.Equals("部长"))
            {
                foreach (String department in emplyee.Departments)
                {
                    Int32 oldMinisterId = this.FindMinisterIdByDepartmentName(department);
                    if (oldMinisterId != Emplyee.IllegalId)
                    {
                        Console.WriteLine("{0} is the minister of {1}, you wanna change? (Y/N)", oldMinisterId, department);
                        String choice = String.Empty;
                        choice = Console.ReadLine();
                        if(choice.Trim().ToUpper().Equals("N"))
                        {
                            emplyee.Departments.Remove(department);
                            if(emplyee.Departments.Count == 0)
                            {
                                emplyee.Departments.Add("员工");
                            }
                            this.UpdateEmplyee(emplyee);
                        }
                    }
                    this.SetMinister(department, emplyee.Id);
                }
            }

        }
        private void AddEmplyee(Int32 number, String name, Boolean gender, String department, String title)
        {
            using (SqlConnection conn = new SqlConnection(this.connString))
            {
                SqlCommand cmd = new SqlCommand(this.cmdAddEmplyee, conn);
                SqlParameter numberPara = new SqlParameter("@Id", number);
                SqlParameter namePara = new SqlParameter("@Name", name);
                SqlParameter genderPara = new SqlParameter("@Gender", gender);
                SqlParameter deparmPara = new SqlParameter("@Department", department);
                SqlParameter titlePara = new SqlParameter("@Title", title);
                conn.Open();
                cmd.Parameters.Add(numberPara);
                cmd.Parameters.Add(namePara);
                cmd.Parameters.Add(genderPara);
                cmd.Parameters.Add(deparmPara);
                cmd.Parameters.Add(titlePara);

                Int32 effectLine = cmd.ExecuteNonQuery();
                if (effectLine == 1)
                {
                    Console.WriteLine("{0} {1} {2} {3} {4} add successfully", number, name, gender, department, title);
                }
                else
                {
                    Console.WriteLine("Something Wrong!");
                }
            }
        }
        public void AddManagement(Int32 id, String department)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdAddManegement, conn);
            SqlParameter idPara = new SqlParameter("@Id", id);
            SqlParameter departmentPara = new SqlParameter("@Department", department);
            conn.Open();

            cmd.Parameters.Add(idPara);
            cmd.Parameters.Add(departmentPara);

            Int32 effectLine = cmd.ExecuteNonQuery();
            if (effectLine == 1)
            {
                Console.WriteLine("{0} {1} add successfully", id, department);
            }
            conn.Dispose();
        }
        private void DeleteManagement(String department)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdDelectManagementByDepartment, conn);
            SqlParameter idPara = new SqlParameter("@Department", department);
            conn.Open();

            cmd.Parameters.Add(idPara);
            Int32 effectLine = cmd.ExecuteNonQuery();
            if (effectLine == 1)
            {
                Console.WriteLine("{0} Delect successfully", department);
            }
            conn.Dispose();
        }
        public void UpdateManagement(String department, Int32 id)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdUpdateManagement, conn);
            SqlParameter idPara = new SqlParameter("@Id", id);
            SqlParameter departmentPara = new SqlParameter("@Department", department);

            conn.Open();
            cmd.Parameters.Add(idPara);
            cmd.Parameters.Add(departmentPara);

            cmd.ExecuteNonQuery();
            Console.WriteLine("{0} line effecr", cmd.ExecuteNonQuery());

            conn.Dispose();
        }
        public void QueryAllApartmentInfo()
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(cmdQueryApartment, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            Dictionary<String, String> dictionaryApartment = new Dictionary<String, String>();
            while (reader.Read())
            {
                dictionaryApartment.Add(reader["Name"].ToString(), reader["Introduction"].ToString());
            }
            conn.Dispose();
            foreach (KeyValuePair<String, String> keyValuePair in dictionaryApartment)
            {
                Int32 id = FindMinisterIdByDepartmentName(keyValuePair.Key);
                String minister = String.Empty;
                if (id != Emplyee.IllegalId)
                {
                    minister = FindEmplyeeNameById(id);
                }
                else
                {
                    minister = "未知";
                }
                Console.WriteLine("{0}\t{1}\t{2}", keyValuePair.Key, keyValuePair.Value, minister);
            }
        }

        public Int32 FindMinisterIdByDepartmentName(String departmentName)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmdSelectId = new SqlCommand(this.cmdSelectMinisterIdByDepartment, conn);
            SqlParameter namePara = new SqlParameter("@DepartmentName", departmentName);
            cmdSelectId.Parameters.Add(namePara);
            conn.Open();

            String id = String.Empty;
            Object objtmp = cmdSelectId.ExecuteScalar();
            if (objtmp != null)
            {
                id = objtmp.ToString();
            }
            conn.Dispose();
            if (id.Length != 0)
            {
                Int32 idInt;
                try
                {
                    idInt = Int32.Parse(id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    idInt = Emplyee.IllegalId;
                }
                return idInt;
            }
            else
            {
                return Emplyee.IllegalId;
            }
        }
        public void ChangeTitle(Int32 id, String Title, String departmentName)
        {
            Emplyee emplyee = this.FindEmplyeeById(id);
            emplyee.Title = Title;
            this.UpdateEmplyee(emplyee);
        }
        public void UpdateEmplyee(Emplyee emplyee)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdUpdateEmplyee, conn);
            SqlParameter namePara = new SqlParameter("@Name", emplyee.Name);
            SqlParameter genderPara = new SqlParameter("@Gender", emplyee.Gender);
            SqlParameter departmentPara = new SqlParameter("@Department", emplyee.GetDepartmentsString());
            SqlParameter titlePara = new SqlParameter("@Title", emplyee.Title);

            conn.Open();

            cmd.Parameters.Add(namePara);
            cmd.Parameters.Add(genderPara);
            cmd.Parameters.Add(departmentPara);
            cmd.Parameters.Add(titlePara);

            Console.WriteLine("{0} line effect", cmd.ExecuteNonQuery());

            conn.Dispose();
        }
        public String GetMemberMinister(Int32 id)
        {
            Emplyee emplyee = this.FindEmplyeeById(id);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("部长员工编号\t部长姓名\n");
            if (emplyee.Title.Equals("部长"))
            {
                stringBuilder.Append(String.Format("{0}\t\t{1}", id, emplyee.Name));
            }
            else
            {
                Int32 ministerId = this.FindMinisterIdByDepartmentName(emplyee.Departments[0]);
                String ministerName = this.FindEmplyeeNameById(ministerId);
                stringBuilder.Append(String.Format("{0}\t\t{1}", ministerId, ministerName));
            }

            return stringBuilder.ToString();
        }
        public void SetMinister(String department, Int32 id)
        {
            Int32 oldMinisterId = this.FindMinisterIdByDepartmentName(department);
            Emplyee oldMinister = this.FindEmplyeeById(oldMinisterId);
            if (oldMinister.Departments.Count > 1)
            {
                oldMinister.Departments.Remove(department);
            }
            else
            {
                oldMinister.Departments[0] = "员工";
            }
            this.UpdateEmplyee(oldMinister);
            this.ChangeTitle(id, "部长", department);
            this.UpdateManagement(department, id);
            Console.WriteLine("Set {0} minister as {1} successfully", department, id);
        }
        public void ChangeDepartment(Int32 id, String departmentName)
        {
            Emplyee emplyee = this.FindEmplyeeById(id);
            emplyee.Departments.Add(departmentName);
            this.UpdateEmplyee(emplyee);
        }
        public void AddDepartmentToId(Int32 id, String departmentName)
        {
            Emplyee emplyee = this.FindEmplyeeById(id);
            if (emplyee == null)
            {
                return;
            }
            emplyee.Departments.Add(departmentName);
            this.UpdateEmplyee(emplyee);
        }
        public Emplyee FindEmplyeeById(Int32 id)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmdSelectEmplyee = new SqlCommand(this.cmdSelectEmplyeeById, conn);
            SqlParameter idPara = new SqlParameter("@Id", id);
            cmdSelectEmplyee.Parameters.Add(idPara);

            conn.Open();

            SqlDataReader reader = cmdSelectEmplyee.ExecuteReader();
            Emplyee emplyee = null;
            while (reader.Read())
            {
                emplyee = new Emplyee();
                try
                {
                    emplyee.Id = Int32.Parse(reader["Id"].ToString());
                    emplyee.Gender = Boolean.Parse(reader["Gender"].ToString());
                }
                catch (Exception ex)
                {
                    //logger.Error(":Parse Error, Error Message: {0}", ex.ToString());
                    Console.WriteLine("################{0}", ex.ToString());
                }

                emplyee.Name = reader["Name"].ToString();
                emplyee.Departments.Add(reader["Department"].ToString());
                emplyee.Title = reader["Title"].ToString();
            }
            conn.Dispose();
            if (emplyee == null)
            {
                return null;
            }
            return emplyee;
        }
        public String FindEmplyeeNameById(Int32 id)
        {
            Emplyee emplyee = this.FindEmplyeeById(id);
            String emplyeeName = emplyee.Name;
            if (emplyeeName.Length != 0)
            {
                return emplyeeName;
            }
            else
            {
                return "未知";
            }
        }
        public void DeleteEmplyeeById(Int32 id)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmdDeleteEmp = new SqlCommand(this.cmdDeleteEmplyeeById, conn);
            SqlParameter idPara = new SqlParameter("@Id", id);
            cmdDeleteEmp.Parameters.Add(idPara);

            conn.Open();

            cmdDeleteEmp.ExecuteNonQuery();
            conn.Dispose();
        }
        public String GetMinisterManagementInfoById(Int32 id)
        {
            List<Department> listDepartments = this.GetDepartmentListByMinisterId(id);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("部分名称\t部门简介\n");
            foreach (Department departmentTmp in listDepartments)
            {
                stringBuilder.Append(String.Format("{0}\t\t{1}\n", departmentTmp.Name, departmentTmp.Introduction));
            }
            return stringBuilder.ToString();
        }
        public List<Department> GetDepartmentListByMinisterId(Int32 id)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdSelectManagementById, conn);
            SqlParameter idPara = new SqlParameter("@Id", id);

            conn.Open();
            cmd.Parameters.Add(idPara);
            SqlDataReader reader = cmd.ExecuteReader();
            List<Department> listDepartments = new List<Department>();
            while (reader.Read())
            {
                Department department = new Department();
                try
                {
                    department.MinisterId = Int32.Parse(reader["Id"].ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                department.Name = reader["Department"].ToString();

                listDepartments.Add(department);
            }
            conn.Dispose();

            for (Int32 i = 0; i < listDepartments.Count; i++)
            {
                String introduction = FindDepartmentByName(listDepartments[i].Name);
                listDepartments[i].Introduction = introduction;
            }
            return listDepartments;
        }
        public Department GetDepartmentByName(String name)
        {
            String introduction = this.FindDepartmentByName(name);
            if (introduction == null)
            {
                return null;
            }
            Department department = new Department();
            department.Introduction = introduction;
            department.Name = name;
            return department;
        }
        public String FindDepartmentByName(String name)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdSelectDepartmentByName, conn);
            SqlParameter namePara = new SqlParameter("@Name", name);

            conn.Open();
            cmd.Parameters.Add(namePara);
            String introduction = (String)cmd.ExecuteScalar();
            conn.Dispose();

            if (introduction == null || introduction.Length == 0)
            {
                return null;
            }
            return introduction;
        }
        public String GetEmplyeeInfoById(Int32 id)
        {
            Emplyee emplyee = this.FindEmplyeeById(id);
            Int32 ministerId;
            String minister = String.Empty;
            if (!emplyee.Title.Equals("部长"))
            {
                ministerId = this.FindMinisterIdByDepartmentName(emplyee.GetDepartmentsString());

                if (ministerId != Emplyee.IllegalId)
                {
                    minister = this.FindEmplyeeNameById(ministerId);
                }
                else
                {
                    minister = "未知";
                }
            }
            else
            {
                minister = emplyee.Name;
            }
            String Info = String.Format("{0}\t{1}\t{2}\t{3}\t\t\t{4}\t{5}",
                emplyee.Id, emplyee.Name, emplyee.Gender, emplyee.GetDepartmentsString(), emplyee.Title, minister);

            return Info;
        }
        public void DeleteDepartment(String department)
        {
            List<Emplyee> listEmplyee = this.FindEmplyeeByDepartment(department);
            foreach (Emplyee emplyee in listEmplyee)
            {
                this.ChangeDepartment(emplyee.Id, "未知");
            }
            this.DeleteManagement(department);
        }
        public List<Emplyee> FindEmplyeeByDepartment(String department)
        {
            SqlConnection conn = new SqlConnection(this.connString);
            SqlCommand cmd = new SqlCommand(this.cmdSelectIdByDepartment, conn);
            SqlParameter departmentName = new SqlParameter("@Department", department);

            conn.Open();
            cmd.Parameters.Add(departmentName);
            SqlDataReader reader = cmd.ExecuteReader();
            List<Emplyee> listEmplyee = new List<Emplyee>();
            while (reader.Read())
            {
                Int32 id = Int32.Parse(reader["Id"].ToString());
                listEmplyee.Add(this.FindEmplyeeById(id));
            }

            return listEmplyee;
        }
    }
}
