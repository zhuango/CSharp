﻿namespace HomeWorkADO
{
    using System;
    using System.Configuration;
    using System.Data.SqlClient;

    class Program
    {
        private static Company company = null;
//        private String cmdCreateEmplyeesDB = @"
//            CREATE TABLE [dbo].[Emplyees](
//	            [Id] [int] NOT NULL,
//	            [Name] [nvarchar](50) NOT NULL,
//	            [Gender] [bit] NOT NULL,
//	            [Department] [nvarchar](50) NOT NULL,
//	            [Title] [nvarchar](100) NOT NULL,
//             CONSTRAINT [PK_Emplyees] PRIMARY KEY CLUSTERED 
//            (
//	            [Id] ASC
//            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
//            ) ON [PRIMARY]";

//        private String cmdCreateDepartmentDB = @"
//            CREATE TABLE [dbo].[Department](
//	            [Name] [nvarchar](50) NOT NULL,
//	            [Intruduction] [nvarchar](100) NOT NULL
//            ) ON [PRIMARY]";

//        private String cmdCreateManagementDB = @"
//            CREATE TABLE [dbo].[Management](
//	            [Id] [int] NOT NULL,
//	            [Departments] [nvarchar](200) NOT NULL,
//             CONSTRAINT [PK_Management] PRIMARY KEY CLUSTERED 
//            (
//	            [Id] ASC
//            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
//            ) ON [PRIMARY]";

        static void Main(string[] args)
        {
            try
            {
                company = new Company();
                //AddDepartment();
                //AddEmplyee();
                company.QueryAllApartmentInfo();
                //company.ChangeDepartment(6, "DEV_2");
                //company.AddDepartmentToId(2, "DEV_2");

                //Console.WriteLine(company.GetEmplyeeInfoById(6));
                //Console.WriteLine(company.GetEmplyeeInfoById(2));
                //Console.WriteLine(company.GetMinisterManagementInfoById(2));
                //company.SetMinister("DEV_2", 5);
                //Console.WriteLine(company.GetMemberMinister(6));
                //company.DeleteDepartment("DEV_2");
 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        static void AddDepartment()
        {
            String choice;
            Console.WriteLine("Add department?(Y/N)");
            choice = Console.ReadLine();
            while (!choice.Trim().ToUpper().Equals("N"))
            {
                Console.WriteLine("Input department name: ");
                String name = Console.ReadLine();
                Console.WriteLine("Input department intruduction: ");
                String intr = Console.ReadLine();
                company.AddDepartment(name, intr);
                Console.WriteLine("You wanna add other one?(Y/N)");
                choice = Console.ReadLine();
            }
        }
        static void AddEmplyee()
        {
            String choice;
            Console.WriteLine("Add Emplyee?(Y/N)");
            choice = Console.ReadLine();
            while (!choice.Trim().ToUpper().Equals("N"))
            {
                Emplyee emplyee = new Emplyee();
                Console.WriteLine("Input Number: ");
                emplyee.Id = Int32.Parse(Console.ReadLine());

                Console.WriteLine("Input Name: ");
                emplyee.Name = Console.ReadLine();

                Console.WriteLine("Input Gender: ");
                String gender = Console.ReadLine();
                if (gender.Equals("男"))
                {
                    emplyee.Gender = true;
                }
                else
                {
                    emplyee.Gender = false;
                }

                Console.WriteLine("Input Department: ");
                emplyee.Departments.Add(Console.ReadLine());

                Console.WriteLine("Input Title: ");
                emplyee.Title = Console.ReadLine();
                company.AddEmplyee(emplyee);
                Console.WriteLine("You wanna add other one?(Y/N)");
                choice = Console.ReadLine();
            }
        }
    }
}
