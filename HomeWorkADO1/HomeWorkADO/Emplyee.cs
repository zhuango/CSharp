﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkADO
{

    public enum EmplyeeType
    {
        None,
        Member,
        Minister
    }
    public class Emplyee
    {
        public const Int32 IllegalId = -1;
        private Int32 id;
        private String name;
        private Boolean gender;
        private List<String> departments;
        private String title;

        public Emplyee()
        {
            departments = new List<String>();
        }

        public Emplyee(Int32 id, String name, Boolean gender, String addToDepartmentList, String title)
            : base()
        {
            this.id = id;
            this.name = name;
            this.Departments.Add(addToDepartmentList);
            this.title = title;
        }
        public Int32 Id
        {
            get { return id; }
            set { id = value; }
        }
        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        public Boolean Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        public List<String> Departments
        {
            get { return departments; }
            set { departments = value; }
        }
        public String Title
        {
            get { return title; }
            set { title = value; }
        }
        public String GetDepartmentsString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < departments.Count - 1; i++)
            {
                stringBuilder.Append(departments[i]);
                stringBuilder.Append(",");
            }
            stringBuilder.Append(departments[departments.Count - 1]);

            return stringBuilder.ToString();
        }
    }
}
