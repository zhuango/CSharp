﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkADO
{
    public class Department
    {
        private String name;
        private String introduction;
        private Int32 ministerId;
        //private Emplyee minister;
        //private List<Emplyee> emplyees;

        public String Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public String Introduction
        {
            get { return this.introduction; }
            set { this.introduction = value; }
        }
        public Int32 MinisterId
        {
            get { return ministerId; }
            set { ministerId = value; }
        }
    }
}
