using System;

namespace temType
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			System.Nullable<int> nullableInt;
			nullableInt = null;
			Console.WriteLine (nullableInt);
			nullableInt = new System.Nullable<int>(10);
			Console.WriteLine (nullableInt);
			
			//Type? is same as System.Nullable<Type>
			int? op1 = 3;
			int b = (int)op1;
			Console.WriteLine (b);
			Console.WriteLine (op1.Value);
			
			int? op2 = 10;
			int? op3 = op2 - op1;
			Console.WriteLine (op3);
			
			int? op4 = null;
			int? op5 = op4 - op3;
			Console.WriteLine (op5);
			Console.WriteLine ("Hello World!");
		}
	}
}
