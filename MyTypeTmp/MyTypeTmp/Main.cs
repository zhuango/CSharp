using System;

namespace MyTypeTmp
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MyType<int> mt = new MyType<int>();
			
			mt.SetData(1000);
			Console.WriteLine (mt.GetData());
			Console.WriteLine (mt.Data);
			Console.WriteLine ("Hello World!");
		}
	}
}
