using System;

namespace MyTypeTmp
{
	//自定义泛型
	public class MyType<T>
	{
		private T data;
		public MyType ()
		{
		}
		public T Data {
			set 
			{
				Data = value;
			}
			get
			{
				return data;
			}
		}
		public void SetData(T data) {
			this.data = data;
		}
		public T GetData() {
			return data;
		}
	}
}

