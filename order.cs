using System;
namespace oRder {
	struct order {
		public string itemName;
		public int unitCount;
		public double unitCost;
		public double TotalPeace(){ 
			return unitCost * unitCount;
		}
		public string Info(){
			string str;
			str = "Order Information: " + 
				unitCount.ToString() + 
				" " + itemName + " items at $" + 
				unitCost.ToString() + " each, total cost " +
				TotalPeace().ToString("0.0");

			return str;

		}
	}
	class Order {
		static void Main(string[] args) {
			order a = new order();
			a.itemName = "LiuZhuang";
			a.unitCount = 2;
			a.unitCost = 10.2;
			Console.WriteLine(a.TotalPeace());
			string info = a.Info();
			Console.WriteLine(info);
		}
	}
}
