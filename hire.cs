using System;

namespace hire {
	class A {
		virtual public void Write() {
			Console.WriteLine("class A");
		}
	}

	class B: A {
		public override void Write() {
			Console.WriteLine("class B hire A");
		}
		public void Write2() {
			Console.WriteLine("class B");
		}
	}

	public class Hire {
		public static void Main(string[] args) {
			B b = new B();
			b.Write();
			b.Write2();

			//virtual function , So class B's Write()
			A a = new B();
			a.Write();

			A c = new A();
			c.Write();
		}
	}
}
