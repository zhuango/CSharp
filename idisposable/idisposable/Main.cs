using System;

namespace idisposable
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string str = new string("NIMA");
			using(str) {
				str = "NSD";
				Console.WriteLine (str);
			}
			Console.WriteLine (str);
			Console.WriteLine ("Hello World!");
		}
	}
}
