using System;

namespace Collection
{
	public class fruit
	{
		private string name = null;
		public string Name {
			get {
				return name;
			}
			set {
				name = value;
			}
		}
		public fruit ()
		{
			name = "No name";
		}
		public virtual string GetName() {
			return name;
		}
	}
}

