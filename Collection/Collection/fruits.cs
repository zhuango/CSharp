using System;
using System.Collections;
namespace Collection
{
	public class fruits : CollectionBase
	{
		public fruits ()
		{
		}
		//Add elements into the collection
		public void Add(fruit f) {
			List.Add (f);
		}
		//remove elements into the collection
		public void Remove(fruit f) {
			List.Remove (f);
		}
		public fruit this[int index] {
			get {
				return (fruit)List[index];
			}
			set {
				List[index] = value;
			}
		}
	}
}

