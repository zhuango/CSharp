using System;

namespace Collection
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			fruits fc = new fruits();
			Apple a = new Apple();
			Orange o = new Orange();
			fruit f = new fruit();
			
			fc.Add (a);
			fc.Add (o);
			fc.Add (f);
			
			Console.WriteLine (fc[0].GetName ());
			Console.WriteLine (fc[1].GetName ());
			Console.WriteLine (fc[2].GetName ());
			
			fc.Remove(o);
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			foreach(fruit tmp in fc) {
				Console.WriteLine (tmp.GetName());
			}
			Console.WriteLine ("Hello World!");
		}
	}
}
