using System;
using System.Collections;
namespace DirectionTest
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string name = "C++ Primer";
			Book b = new Book(name, "Jos", 1000);
			Console.WriteLine (b.GetMessageAboutBook());
			
			Book c = new Book("C Primer", "LiuZhunag", 1000);
			Console.WriteLine (c.GetMessageAboutBook ());
			
			Book d = new Book("春天的故事", "张开里", 10.12, 10000);
			Console.WriteLine(d.GetMessageAboutBook());
			
			Books books = new Books();
			books.Add (b.GetId (), b);
			books.Add (c.GetId (), c);
			books.Add (d.GetId (), d);
			Console.WriteLine ("###############################");
			
			//Special style of dictionary go through
			foreach(DictionaryEntry book in books) {
				Console.WriteLine( ((Book)book.Value).GetMessageAboutBook());
			}
			Console.WriteLine ("Hello World!");
		}
	}
}
