using System;

namespace DirectionTest
{
	public class Book
	{
		public static int Id = 0;
		private int id = 0;
		private string name = null;
		private string author = null;
		private double price = 0;
		private int pages = 0;
		
		private void SetID() {
			//Get this book a id, then plus id
			id = Id;
			Id++;
		}
		//Init all pro
		public Book(string name, string author, double price, int pages) {
			SetID ();
			this.name = name;
			this.author = author;
			this.price = price;
			this.pages = pages;
		}
		//Init name, author, pages
		public Book(string name, string Author, int Pages) {
			SetID();
			this.name = name;
			author = Author;
			pages = Pages;
		}
		//Init name and pages
		public Book(string Name, int Pages) {
			SetID();
			name = Name;
			pages = Pages;
		}
		//No init, bad init
		public Book ()
		{	
			SetID();
			name = "No name";
			author = "No author";
			price = 0.0;
			pages = 0;
		}
		/// <summary>
		/// Sets the name.
		/// </summary>
		/// <param name='name'>
		/// Name.
		/// </param>
		/// 
		public void SetName(string name) {
			this.name = name;
		}
		/// <summary>
		/// Sets the author.
		/// </summary>
		/// <param name='author'>
		/// Author.
		/// </param>
		public void SetAuthor(string author) {
			this.author = author;
		}
		/// <summary>
		/// Sets the price.
		/// </summary>
		/// <param name='price'>
		/// Price.
		/// </param>
		public void SetPrice(double price) {
			this.price = price;
		}
		/// <summary>
		/// Sets the pages.
		/// </summary>
		/// <param name='pages'>
		/// Pages.
		/// </param>
		public void SetPages(int pages) {
			this.pages = pages;
		}
		/// <summary>
		/// Gets the message about book.
		/// </summary>
		/// <returns>
		/// The message about book.
		/// </returns>
		//Get book message
		public string GetMessageAboutBook() {
			return  "ID: "			+ id	+ "\n" +
					"Book Name: "	+ name	+ "\n" +
					"Author: "		+ author+ "\n" +
					"Price: "		+ price + "\n" + 
					"Page number: " + pages + "\n";
		}
		/// <summary>
		/// Gets the identifier.
		/// </summary>
		/// <returns>
		/// The identifier.
		/// </returns>
		public int GetId() {
			return id;
		}
	}
}

