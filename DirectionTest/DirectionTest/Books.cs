using System;
using System.Collections;

namespace DirectionTest
{
	public class Books : DictionaryBase
	{
		public Books ()
		{
		}
		//Add a element to dictinary
		public void Add(int bookID, Book newBook) {
			Dictionary.Add(bookID, newBook);
		}
		//Remove a element from fictinary
		public void Remove(int oldID) {
			Dictionary.Remove(oldID);
		}
		//String index operation
		public Book this[int strIndex] {
			get {
				return (Book)Dictionary[strIndex];
			}
			set {
				Dictionary[strIndex] = value;
			}
		}
	}
}

