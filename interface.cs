using System;
namespace inter {
	interface inte1 {
		void Write();
	}
	interface inte2 {
		void Write2();
	}

	class Test: inte1, inte2 {
		//must implement
		public void Write() {
			Console.WriteLine("interface inte1 request");
		}
		public void Write2() {
			Console.WriteLine("interface inte2 request");
		}
	}
	class Inter {
		public static void Main(string[] args) {
			Test t = new Test();
			t.Write();
			t.Write2();
		}
	}
}
