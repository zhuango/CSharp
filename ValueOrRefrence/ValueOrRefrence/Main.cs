using System;

namespace ValueOrRefrence
{
	public class A<T> {
		public T a;
		public A() {
			//如果 a 是引用类型,就结它赋予 null 值;
			//如果 a 是值类型,就结它赋予默认值
			a = default(T);
		}
	}
	
	class MainClass
	{
		public static void Main (string[] args)
		{
			
			Console.WriteLine ("Hello World!");
		}
	}
}
