using System;
namespace con {
	class Con {
		delegate string Read();
		static string read() {
			return Console.ReadLine();
		}
		public static void Main(string[] args) {
			Read r = new Read(read);

			int a = Convert.ToInt32(r());
			a = a - 1;
			Console.WriteLine(a);
		}
	}
}
