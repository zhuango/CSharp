using System;

namespace people
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			People ps = new People();
			Person p1 = new Person();
			p1.Name = "LiuZhuang";
			p1.Age = 23;
			Person p2 = new Person();
			p2.Name = "ZhangKaili";
			p2.Age = 23;
			Person p3 = new Person();
			p3.Name = "BaiDonghui";
			p3.Age = 100;
			Person p4 = new Person();
			p4.Age = 100;
			p4.Name = "SHABI";
			Person p5 = new Person();
			p5.Age = 100;
			p5.Name = "JIASD";
			Person p6 = new Person();
			p6.Age = 2;
			p6.Name = "ASD";
			
			ps.Add (p1);
			ps.Add (p2);
			ps.Add (p3);
			ps.Add (p4);
			ps.Add (p5);
			ps.Add (p6);
			
			foreach(int age in ps.AgeList()) {
				Console.WriteLine (age);
			}
			/*
			Person p7 = new Person();
			p7.Age = 10000;
			p7.Name = "Li";
			ps.Add (p7);
			*/
			
			foreach(Person p in ps.GetOldest()) 
				Console.WriteLine (p.GetMessage());
			Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			
			People ps2 = (People) ps.Clone();
			
			Person p7 = new Person();
			p7.Age = 10000;
			p7.Name = "Li";
			ps2.Add (p7);
			
			foreach(Person p in ps2.GetOldest()) 
				Console.WriteLine (p.GetMessage());
			
		}
	}
}
