using System;

namespace people {
	public class Person : ICloneable
	{
		private string name;
		private int age;
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int Age
		{
			get
			{
				return age;
			}
			set
			{
				age = value;
			}
		}
		/// <summary>
		/// Gets the message.
		/// </summary>
		/// <returns>
		/// The message.
		/// </returns>
		public string GetMessage() {
			return "Name : " + Name + " Age: " + Age;
		}
	
		public static bool operator > (Person p1, Person p2) {
			if(p1.Age > p2.Age) {
				return true;
			}else
				return false;
		}
		
		public static bool operator <(Person p1, Person p2) {
			if(p1.Age < p2.Age) {
				return true;
			}else 
				return false;
		}
		
		public static bool operator >= (Person p1, Person p2) {
			if(p1.Age >= p2.Age) {
				return true;
			}else
				return false;
		}
		
		public static bool operator <= (Person p1, Person p2) {
			if(p1.Age <= p2.Age) {
				return true;
			}else
				return false;
		}
		public object Clone() {
			Person p = new Person();
			p.Name = (string)Name.Clone ();
			p.Age = Age;
			
			return p;
		}
		
	}
}
