using System;
using System.Collections;
namespace people
{
	public class People : DictionaryBase, ICloneable
	{
		public People ()
		{
		}
		public void Add(Person p) {
			Dictionary.Add(p.Name, p);
		}
		
		public void Remove(Person p) {
			Dictionary.Remove (p.Name);
		}
		
		public Person this[string index]
		{
			get
			{
				return (Person)Dictionary[index];
			}
			set
			{
				Dictionary[index] = value;
			}
		}
		
		//public IEnumerator GetEnumerator() {
			
		//}
		public Person[] GetOldest() {
			People ps = new People();
			//Init Person object
			Person tmp = new Person();
			tmp.Age = 0;
			tmp.Name = "NO name";
			
			foreach(DictionaryEntry p in this) {
				if((Person)p.Value >= tmp) {
					if(tmp.Age == ((Person)p.Value).Age) {
						ps.Add((Person)p.Value);
					}else {
						ps.Clear ();
						ps.Add ((Person)p.Value);
						tmp = (Person)p.Value;
					}
				}
			}
			Person[] psr = new Person[ps.Count];
			int i = 0;
			foreach(DictionaryEntry p in ps) {
				psr[i] = (Person)p.Value;
				i++;
			}
			return psr;
		}
		
		public object Clone(){
			People p = new People();
			foreach(DictionaryEntry pd in this) {
				//新建对象，添加至新People对象
				Person tmp = new Person();
				tmp = (Person)((Person)pd.Value).Clone ();
				p.Add (tmp);
			}
			return p;
		}
		
		//迭代people对象中的年龄
		public IEnumerable AgeList() {
			//迭代出Person对象
			foreach(DictionaryEntry d in this) {
				Person tmp = new Person();
				tmp = (Person)d.Value;
				//返回Person对象的年龄
				yield return tmp.Age;
			}
		}

	}
}

