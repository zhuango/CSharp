using System;

namespace HidingClassFunc
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			BaseClass bc = new BaseClass();
			bc.Print();
			
			MyClass mb = new MyClass();
			mb.Print();
			
			BaseClass bc2 = new MyClass();
			bc2.Print ();
			bc2.born ();
			//Error
			//MyClass mb2 = new BaseClass();
			Console.WriteLine ("Hello World!");
		}
	}
}
