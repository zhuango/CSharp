using System;

namespace HidingClassFunc
{
	public class BaseClass
	{
		public BaseClass ()
		{
		}
		public void Print() {
			Console.WriteLine ("This is Base Class");
		}
		public virtual void born() {
			Console.WriteLine ("Father is born");
		}
	}
	
	public class MyClass: BaseClass
	{
		new public void Print() {
			Console.WriteLine ("This is My Class");
		}
		public override void born() {
			base.born();
			Console.WriteLine ("Child is born");
		}
		
	}
}

