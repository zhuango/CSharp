using System;

namespace Copy {
	public class copy {
		public static void Main(string[] args) {
			int[] a = new int[11];
			int[] b = new int[10];

			for(int i = 0; i < 10; i++)
				Console.WriteLine(a[i]);
			Console.WriteLine();

			for(int i = 0; i < 10; i++)
				b[i] = i;
			Console.WriteLine();

			b.CopyTo(a, 1);
			for(int i = 0; i < 11; i++)
				Console.WriteLine(a[i]);
		}
	}
}

