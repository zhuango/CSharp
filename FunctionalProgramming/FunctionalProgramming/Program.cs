namespace FunctionalProgramming
{
    using System;

    delegate double MyFunction(double x);
    class MainClass
    {
        public static void delegateFuncObj()
        {
            MyFunction fun = Math.Sin;
            double y = fun(5);
            System.Console.WriteLine(y);
            fun = Math.Exp;
            y = fun(5);
        }
        public static void Printer(int a)
        {
            System.Console.WriteLine(a);
        }
        public static Boolean IsBiggerThanZero(int a)
        {
            if (a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void FuncMethod()
        {
            //Must have return parameter.
            Func<double, double> f = Math.Sin;
            double y = f(4);
            System.Console.WriteLine(y);
            f = Math.Exp;
            y = f(4);
            System.Console.WriteLine(y);

            //No return parameter.
            Action<int> returnVoid = Printer;
            returnVoid(123);

            //bool woubld be the type of return parameter
            Predicate<int> returnBool = IsBiggerThanZero;
            returnBool(12);
            returnBool(-12);

            //anonymous delegates and lambda expression.
            Func<double, double> anonymDelegate = delegate(double x){ return 3 * x + 1;};
            anonymDelegate(12.0);

            Func<double, double> lambdaFunc = (x) => { return 3 * x + 1;};
            lambdaFunc(12.0);

        }
        public static void Main(string[] args)
        {
            //FuncMethod();
            //FunctionArithmetic.Test();
            //CSFunctionalProg.test();
            //UsingLINQ.test();
            //HigherOrderFunctions.test();
            //AsynchronousFunctions.test();
            //Tuples.test();
            //Closures.test();
            Recursion.test();
        }
    }
}
