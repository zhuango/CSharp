﻿
namespace FunctionalProgramming
{
    using System;
    using System.Linq;

    public static class Recursion
    {
        private static int Factorial(int n)
        {
            return n < 1 ? 1 : n * Factorial(n - 1);
        }

        private static Func<int, int> Factorial()
        {
            Func<int, int> factorial = null;
            factorial = n => n < 1 ? 1 : n * factorial(n - 1);
            return factorial;
        }

        public static Func<T2, TR> Partial1<T1, T2, TR>(this Func<T1, T2, TR> func, T1 first)
        {
            return b => func(first, b);
        }

        public static Func<T1, TR> Partial2<T1, T2, TR>(this Func<T1, T2, TR> func, T2 second)
        {
            return b => func(b, second);
        }

        public static void test()
        {
            var f = Factorial();
            for (int i = 0; i < 10; ++i)
            {
                Console.WriteLine("{0}! = {1}", i, f(i));
            }

            var numbers = new[]{ 5, 1, 3, 7, 2, 6, 4 };
            Func<int, int> factorial = delegate(int num)
            {
                Func<int, int> locFactorial = null;
                locFactorial = n => n == 1 ? 1 : n * locFactorial(n - 1);
                return locFactorial(num);
            };
            var smallnums = numbers.Where(n => factorial(n) < 7);
            smallnums.ToList().ForEach(node => Console.WriteLine(node));

            double x;
            Func<double, double, double> pow = Math.Pow;

            Func<double, double> exp = pow.Partial1(Math.E);
            Func<double, double> step = pow.Partial1(2);
            if (exp(4) == Math.Exp(4))
            {
                x = step(5);
            }

            double y;

            Func<double, double> square = pow.Partial2(2);
            Func<double, double> squareroot = pow.Partial2(0.5);
            Func<double, double> cube = pow.Partial2(3);

            y = square(5);
            Console.WriteLine(y);
            y = squareroot(9);
            Console.WriteLine(y);
            y = cube(3);
            Console.WriteLine(y);
        }
    }
}

