﻿namespace FunctionalProgramming
{
    using System;

    public static class CompositionFunction
    {
        private static Func<T1, T3> Compose<T1, T2, T3>(Func<T1, T2> f, Func<T2, T3> g)
        {
            return (x)=> g(f(x));
        }
        public static void test()
        {
            Func<double, double> sin = Math.Sin;
            Func<double, double> exp = Math.Exp;
            Func<double, double> exp_sin = Compose(sin, exp);
            double y = exp_sin(3);
            Console.WriteLine(y);
        }
    }
}

