﻿namespace FunctionalProgramming
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    //Functions that handle other functions are called higher order functions
    public static class HigherOrderFunctions
    {
        //extension method.
        private static IEnumerable<T2> MySelect<T1, T2>(this IEnumerable<T1> data, Func<T1, T2> f)
        {
            List<T2> retVal = new List<T2>();
            foreach (T1 x in data)
            {
                retVal.Add(f(x));
            }
            return retVal;
        }

        public static void test()
        {
            List<int> numbers = new List<int>();
            numbers.Add(12);
            numbers.Add(4);
            numbers.Add(1);
            numbers.Add(7);
            numbers.Add(90);

            List<int> square = numbers.Select(num => num * num).ToList();
            square.ForEach(elem => Console.WriteLine(elem));

            List<int> MySquare = square.MySelect(num => num * num).ToList();
            MySquare.ForEach(elem => Console.WriteLine(elem));

        }
    }
}

