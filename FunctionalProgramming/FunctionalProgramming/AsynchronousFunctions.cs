﻿namespace FunctionalProgramming
{
    using System;
    using System.Threading;
    using System.Runtime.Remoting.Messaging;

    public static class Klass
    {
        public static int SlowFunction(int x, int y)
        {
            Thread.Sleep(10000);
            return x + y;
        }
        public static void SlowFunctionCallBack(IAsyncResult async)
        {
            string arguments = (string)async.AsyncState;
            var ar = (AsyncResult)async;
            var fn = (Func<int, int, int>)ar.AsyncDelegate;

            int result = fn.EndInvoke(async);
            Console.WriteLine("f({0}) = {1}", arguments, result);
        }
    }
//    Each function object in C# has the following methods:
//
//    BeginInvoke that starts function execution but does not wait for the function to finish,
//    IsCompleted that checks whether the execution is finished,
//    EndInvoke that blocks execution of the current thread until the function is completed.
    public static class AsynchronousFunctions
    {
        public static void test()
        {
            Func<int, int, int> f = Klass.SlowFunction;
            //The two null:
            //1 Callback function that will be called when the function finishes. return void.
            //2 Some object that will be passed to the callback function.
            IAsyncResult async = f.BeginInvoke(5, 3, null, null);

            while (!async.IsCompleted)
            {
                Thread.Sleep(100);
            }
            int sum = f.EndInvoke(async);
            Console.WriteLine(sum);

            //no need to check the completion.
            f.BeginInvoke(5, 3, Klass.SlowFunctionCallBack, "5, 3");
            Thread.Sleep(20000);
        }
    }
}

