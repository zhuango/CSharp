﻿using System;

namespace FunctionalProgramming
{
    public static class Tuples
    {
        private static Random rnd = new Random();

        private static Tuple<int, string, bool, int> CreateBranchOffice(Tuple<int, string, bool, int> company)
        {
            var branch = Tuple.Create(1, company.Item2, company.Item3, 123);
            Console.WriteLine(company);
            branch = Tuple.Create(10 * company.Item1 + 1, company.Item2 + " Office", true, company.Item1);
            Console.WriteLine(branch);
            Tuple<int, string, bool, int> office = Tuple.Create(branch.Item1,
                branch.Item2,
                branch.Item3,
                company.Item4);
            return office;
        }

        private static Tuple<double, double> CreateRandomPoint()
        {
            var x = rnd.NextDouble() * 10;
            var y = rnd.NextDouble() * 10;
            return Tuple.Create(x, y);
        }

        public static void test()
        {
            Predicate<Tuple<double, double>> isInCircle;
            isInCircle = t => (t.Item1 * t.Item1 + t.Item2 * t.Item2 < 1);

            for (int i = 0; i < 100; ++i)
            {
                var point = CreateRandomPoint();
                if (isInCircle(point))
                {
                    Console.WriteLine("Point {0} in placed within the circle with radiuc 1", point);
                }
            }

            var office = CreateBranchOffice(Tuple.Create(1, "DUT", true, 123));
            Console.WriteLine(office);
        }
    }
}

