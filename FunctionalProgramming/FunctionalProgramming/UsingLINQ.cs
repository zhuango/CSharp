namespace FunctionalProgramming
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class UsingLINQ
    {
        public static void test()
        {
            Predicate<string> longWords = delegate(string word)
            {
                return word.Length > 10;
            };

            List<string> words = new List<string>();
            words.Add("liuzhuang");
            words.Add("                ");
            words.Add("3333333333333333333333333");
            words.Add("111");
            words.Add("223d");

            List<string> longwords = words.FindAll(longWords);
            longwords.ForEach(node => Console.WriteLine(node));

            string[] wordss = new string[] { "C#", ".NET", "ASP.NET", "MVC", " ", "Visual Studio" };
            Func<string, char> firstLetter = delegate (string s) {return s[0];};
            List<string> sorted = wordss.OrderBy(word => word.Length).ThenBy(firstLetter).ToList();
            sorted.ForEach(node => Console.WriteLine(node));

            string[] words2 = new string[] { "C#", ".NET", null, "MVC", "", "Visual Studio" };
            int num = words2.Count( String.IsNullOrEmpty );
            Console.WriteLine(num);
        }
    }
}

