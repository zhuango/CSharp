
namespace FunctionalProgramming
{
    using System;
    using System.IO;
    using System.Collections.Generic;

    public static class ExtensionMethod
    {
        //extension method.
        public static void Use<T>(this T obj, Action<T> action) where T: IDisposable
        {
            using (obj)
            {
                action(obj);
            }
        }
    }
    public static class CSFunctionalProg
    {

        private static void streanWriterMethod(StreamWriter sw)
        {
            sw.WriteLine("liuzhuang is here");
        }
        private static int Count<T>(T[] arr, Predicate<T> condition)
        {
            int counter = 0;
            for (int i = 0; i < arr.Length; ++i)
            {
                if (condition(arr[i]))
                {
                    counter ++;
                }
            }
            return counter;
        }
        public static void test()
        {   
            Predicate<string> longWords = delegate(string word)
            {
                return word.Length > 10;
            };
            string[] words = new string[4];
            words[0] = "liuzuhang";
            words[1] = "weeeeeeeeeeeeeeeeeee";
            words[2] = "s";
            words[3] = "qwwwwwwwwwwwwwwwwww";
            int numberOfBookWithLongNames = Count(words, longWords);
            Console.WriteLine(numberOfBookWithLongNames);

            File.CreateText("../../test").Use(streanWriterMethod);
            File.CreateText("../../test").Use(x => x.WriteLine("lambda"));
            File.CreateText("../../test").Use(delegate(StreamWriter sw) {sw.WriteLine("In delegate.");});

            List<string> names = new List<string>();
            names.Add("liuzhuang");
            names.Add("kailziahgn");
            names.Add("wanglei");
            names.ForEach(node => Console.WriteLine(node));

        }
    }
}

