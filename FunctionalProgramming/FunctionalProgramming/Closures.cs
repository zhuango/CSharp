﻿
namespace FunctionalProgramming
{
    using System;
    using System.Threading;
    
    public static class Closures
    {
        private static Func<int, int> ReturnClousureFunction()
        {
            int val = 0;
            Func<int, int> add = delegate(int delta)
            {
                val += delta;
                return val;
            };

            val = 10;
            return add;
        }
        private static Tuple<Action, Action, Action> CreateBoundFunction()
        {
            int val2 = 0;
            Action increment = ()=>val2++;
            Action decrement = delegate() {
                val2--;
            };
            Action print = () => Console.WriteLine("val2 = " + val2);

            return Tuple.Create(increment, decrement, print);
        }
        private static Func<T> Cache<T>(this Func<T> func, int cacheInterval)
        {
            var cachedValue = func();
            var timecached = DateTime.Now;

            Func<T> cachedFunc = () =>
            {
                if ((DateTime.Now - timecached).Seconds >= cacheInterval)
                {
                    timecached = DateTime.Now;
                    cachedValue = func();
                }
                return cachedValue;
            };

            return cachedFunc;
        }
        public static void test()
        {
            int val = 0;
            Func<int, int> add = delegate(int delta) { val += delta; return val;};
            val = 10;
            var x = add(5);
            var y = add(7);

            Func<int, int> addClousure = ReturnClousureFunction();
            x = addClousure(5);
            y = addClousure(7);

            ////////////////////////////////
            var tuple = CreateBoundFunction();
            Action increment = tuple.Item1;
            Action decrement = tuple.Item2;
            Action print = tuple.Item3;

            increment();
            print();
            increment();
            print();
            increment();
            print();
            decrement();
            print();

            Func<DateTime> now = () => DateTime.Now;
            Func<DateTime> nowCached = now.Cache(4);

            Console.WriteLine("\tCurrent time\tCached time");
            for (int i = 0; i < 20; ++i)
            {
                Console.WriteLine("{0}.\t{1:T}\t{2:T}", i + 1, now(), nowCached());
                Thread.Sleep(1000);
            }
        }
    }
}

