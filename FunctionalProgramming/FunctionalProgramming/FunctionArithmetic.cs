namespace FunctionalProgramming
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public static class FunctionArithmetic
    {
        private static void Hello(string s)
        {
            System.Console.WriteLine("  Hello, {0}!", s);
        }
        private static void Goodbye(string s)
        {
            System.Console.WriteLine("  Goodbyte, {0}!", s);
        }
        public static void Test()
        {
            Action<string> action = Console.WriteLine;
            Action<string> hello = Hello;
            Action<string> goodbye = Goodbye;

            action += Hello;
            action += (x) => { Console.WriteLine("  Greating {0} from lambda expression", x); };
            action("First");

            action -= hello;
            action("Second");

            action = Console.WriteLine + goodbye
                + delegate(string x)
            {
                Console.WriteLine(" Greating {0} from delegate", x);
            };
            action("Third");

            (action - Goodbye)("Fourth");

            foreach (Delegate del in action.GetInvocationList())
            {
                Console.WriteLine(del.Method.Name);
            }
        }
    }
}

