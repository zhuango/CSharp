using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace chapter11_2_1
{
	interface IMyInterface {
	}
	//实现了接口的结构
	public struct MyStruct : IMyInterface {
		public int data;
		public int GetData() {
			return data;
		}
		public void SetData(int a) {
			data = a;
		}
	}
	//一个类
	public class MyClass {
		public int data = 10;
	}
	//一个结构
	public struct MyStruct2 {
		public int data;
		
		public int GetData() {
			return data;
		}
		public void SetData(int a) {
			data = a;
		}
	}
	//一个实现了接口的类
	public class MyClass2 : IMyInterface {
		public int data = 100;
	}
	
	class MainClass
	{

		public static void Main (string[] args)
		{
			//首先,它允许在项的类型是 object 的集合(如 ArrayList)中使用值类型。
			//其次,有一个内部机制允许在值类型上调用object,例如 int 和结构。
			//Note: 最后需要注意的是,在访问值类型内容前,必须进行拆箱。
			
			//Mystruct2结构没有实现接口，封箱是 源对象的一个副本的引用（结构是值类型）
			MyStruct2 a = new MyStruct2();
			a.SetData(12);
			//封箱
			object o2 = a;
			//拆箱
			MyStruct2 b = (MyStruct2)o2;
			a.SetData(13);
			//源对象的一个副本的引用（结构是值类型）
			Console.WriteLine (a.GetData ());
			Console.WriteLine (b.GetData ());
			
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			//MyStruct给结构实现了接口， 封箱是 源对象的一个副本的引用（结构是值类型）
			MyStruct ms = new MyStruct();
			ms.SetData (1024);
			//封箱
			IMyInterface I = ms;
			//封箱
			MyStruct ms2 = (MyStruct)I;
			ms.SetData(2048);
			//源对象的一个副本的引用（结构是值类型）
			Console.WriteLine(ms2.GetData());
			Console.WriteLine (ms.GetData());
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			
			//Mylass该类没有实现接口, 封箱是 一个引用类型赋予了对象，引用了相同的对象(类是引用类型)
			MyClass mc = new MyClass();
			//封箱
			object o = mc;
			//拆箱
			MyClass mc2 = (MyClass) o;
			mc.data = 100;
			//引用了相同的对象(类是引用类型)
			Console.WriteLine(mc2.data);
			Console.WriteLine (mc.data);
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			
			//一个实现了接口的类，封箱是 一个引用类型赋予了对象，引用了相同的对象(类是引用类型)
			MyClass2 d = new MyClass2();
			//封箱
			IMyInterface i2 = d;
			//拆箱
			MyClass2 e = (MyClass2)i2;
			d.data = 120;
			//引用了相同的对象(类是引用类型)
			Console.WriteLine (d.data);
			Console.WriteLine (e.data);
		}
	}
}