using System;

namespace cardlib2
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Deck myDeck = new Deck();
			myDeck.Shuffle();
			for (int i = 0; i < 52; i++)
			{
				Card tempCard = myDeck.GetCard(i);
				Console.Write(tempCard.ToString());
				if (i != 51)
					Console.Write(", ");
				else
					 Console.WriteLine();
			}
			myDeck.Shuffle();
			myDeck.Flush ();
			myDeck.Shuffle();
			
			myDeck.Flush ();
			Console.ReadKey();
		}
	}
}
