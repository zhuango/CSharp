using System;
using System.Collections;
namespace cardlib2
{
	public class Cards : CollectionBase
	{
		public Cards ()
		{
		}
		//Add new element to collection
		public void Add(Card newCard) {
			List.Add (newCard);
		}
		//Remoce old element to collection
		public void Remove(Card oldCard) {
			List.Add (oldCard);
		}
		//index operation
		public Card this[int index] {
			get
			{
				return (Card)List[index];
			}
			set
			{
				List[index] = value;
			}
		}
		//Copy this project to a target project
		public void CopyTo(Cards targetCards) {
			for(int i = 0; i < this.Count; i++)
				targetCards[i] = this[i];
		}
		//Contain a particular
		public bool Contains(Card card) {
			return InnerList.Contains(card);
		}
	}
}

