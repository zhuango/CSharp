﻿namespace HomeWorkBackup
{
    using System;
    using System.Xml;
    using System.IO;

    public class XMLBackuper
    {
        private XmlDocument xmlDocument = null;
        private String xmlPath = null;
        private String sourcePath = String.Empty;
        private String destinPath = String.Empty;
        //logger
        private ILogger logger = FileLogger.GetInstance();
        private DriveInfo driverInfo = null;

        public XMLBackuper() :
            this(@"C:\Windows\Temp\FileInfo.xml")
        {
        }

        public XMLBackuper(String xmlPath)
        {
            if (!Directory.Exists(xmlPath))
            {
                logger.Warning(":Source Directory \"{0}\" was not found, Use the {1}", xmlPath, @"C:\Windows\Temp\FileInfo.xml");
                xmlPath = @"C:\Windows\Temp\FileInfo.xml";
            }
            this.xmlPath = xmlPath;
        }

        public String DestinPath
        {
            get
            {
                return this.destinPath;
            }
        }
        public void Restore(String path)
        {
            if (path.EndsWith("\\"))
            {
            }
            else
            {
                path = path + "\\";
            }
            this.destinPath = path;
            this.driverInfo = new DriveInfo(path.Substring(0, 1));

            try
            {
                //Be sure the root directory is created
                DirectoryInfo directoryInfo = Directory.CreateDirectory(path);
                logger.Info(":Directory: {0} was Created", path);
                if (xmlDocument == null)
                {
                    xmlDocument = new XmlDocument();
                    try
                    {
                        xmlDocument.Load(xmlPath);
                    }
                    catch (FileNotFoundException fileNotFoundException)
                    {
                        logger.Error(":Backup xml File {0} was not found, Error Message, {1}", xmlPath, fileNotFoundException.ToString());
                        return;
                    }
                }
                restore(path, xmlDocument.FirstChild);
                this.SetFileInfo(directoryInfo, xmlDocument.FirstChild);
            }
            catch (IOException IOException)
            {
                logger.Error(":Directory: {0}, Error Message: {1}", path, IOException.ToString());
                return;
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                logger.Error(":Directory: {0} {1}", path, unauthorizedAccessException.ToString());
                return;
            }
            catch (Exception e)
            {
                logger.Error(":Directory: {0} {1}", path, e.ToString());
                return;
            }

        }
        public void restore(String path, XmlNode xmlNodeArg)
        {
            foreach (XmlNode xmlNode in xmlNodeArg.ChildNodes)
            {
                if (xmlNode.Name.Equals("File"))
                {
                    String fileFullName = String.Format("{0}{1}", path, xmlNode.InnerText);
                    //Be sure available free space is big enough
                    foreach(XmlAttribute xmlAttribute in xmlNode.Attributes)
                    {
                        if (xmlAttribute.Name.Equals("Size"))
                        {
                            if (int.Parse(xmlAttribute.Value) > this.driverInfo.AvailableFreeSpace)
                            {
                                logger.Error(":File {0} can not be created, Error Message: Insufficient Disk Space, Drive \"{1}\" AvailableFreeSpace {2}", fileFullName, this.driverInfo.Name, this.driverInfo.AvailableFreeSpace);
                                return;
                            }
                        }
                    }
                    try
                    {
                        //Create file, use using candy for calling dispose auto(write content of file if need)
                        using (FileStream fileStraem = new FileStream(fileFullName, FileMode.Create))
                        { }

                        FileInfo fileInfo = new FileInfo(fileFullName);
                        this.SetFileInfo(fileInfo, xmlNode);
                        logger.Info(":File {0} was created", fileFullName);
                    }
                    catch (IOException IOException)
                    {
                        logger.Error(":File {0} was not found ErrorMessge: {1}", fileFullName, IOException.ToString());
                    }
                }
                else if (xmlNode.Name.Equals("Folder"))
                {
                    String directoryFullName = String.Format("{0}{1}\\", path, xmlNode.Attributes[2].Value);
                    try
                    {
                        DirectoryInfo directoryInfo = Directory.CreateDirectory(directoryFullName);
                        logger.Info(":Directory: {0} was Created", directoryFullName);
                        restore(directoryFullName, xmlNode);
                        this.SetFileInfo(directoryInfo, xmlNode);
                    }
                    catch (IOException IOException)
                    {
                        logger.Error(":Directory: {0} was not found, Error Message: {1}", path, IOException.ToString());
                    }
                    catch (UnauthorizedAccessException unauthorizedAccessException)
                    {
                        logger.Error(":Directory: {0} was not found, Error Message: {1}", path, unauthorizedAccessException.ToString());
                    }
                    catch (Exception e)
                    {
                        logger.Error(":Directory: {0} was not found, Error Message: {1}", path, e.ToString());
                    }
                }
            }
        }

        private void SetFileInfo(FileSystemInfo fileSystemInfo, XmlNode xmlNode)
        {
            foreach (XmlAttribute xmlAttribute in xmlNode.Attributes)
            {
                if (xmlAttribute.Name.Equals("CreateTime"))
                {
                    fileSystemInfo.CreationTime = Convert.ToDateTime(xmlAttribute.Value);
                }
                else if (xmlAttribute.Name.Equals("ModifyTime"))
                {
                    fileSystemInfo.LastWriteTime = Convert.ToDateTime(xmlAttribute.Value);
                }
            }
        }

        public void Traversal(String path)
        {
            if (path.EndsWith("\\"))
            {
            }
            else
            {
                path = path + "\\";
            }
            if (!Directory.Exists(path))
            {
                logger.Error(":Directory {0} was not found", path);
                return;
            }
            this.sourcePath = path;
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            this.xmlDocument = new XmlDocument();

            //Create root node
            var rootNode = this.GetXmlNode(directoryInfo, "Folder");

            var folderName = xmlDocument.CreateAttribute("Name");
            folderName.Value = directoryInfo.Name;
            rootNode.Attributes.Append(folderName);

            xmlDocument.AppendChild(rootNode);

            this.traversal(path, rootNode);
            xmlDocument.Save(xmlPath);
        }

        private void traversal(String path, XmlNode xmlNode)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            try
            {
                foreach (FileInfo fileInfo in directoryInfo.GetFiles())
                {
                    var fileNode = this.GetXmlNode(fileInfo, "File");
                    //Add size of file or directory
                    var sizeNode = xmlDocument.CreateAttribute("Size");
                    sizeNode.Value = fileInfo.Length.ToString();
                    fileNode.Attributes.Append(sizeNode);

                    xmlNode.AppendChild(fileNode);
                }
            }
            catch (DirectoryNotFoundException directoryNotFoundException)
            {
                logger.Error(":Directory {0} was not found, Error Message:", path, directoryNotFoundException.ToString());
            }
            try
            {
                foreach (DirectoryInfo directoryInfoTmp in directoryInfo.GetDirectories())
                {
                    var folderNode = this.GetXmlNode(directoryInfoTmp, "Folder");
                    //Add Name attribute
                    var folderName = xmlDocument.CreateAttribute("Name");
                    folderName.Value = directoryInfoTmp.Name;
                    folderNode.Attributes.Append(folderName);

                    xmlNode.AppendChild(folderNode);
                    this.traversal(directoryInfoTmp.FullName, folderNode);
                }
            }
            catch (DirectoryNotFoundException directoryNotFoundException)
            {
                logger.Error(":Directory {0} was not found, Error Message:", path, directoryNotFoundException.ToString());
            }
            catch (UnauthorizedAccessException unauthorizedAccessException)
            {
                logger.Error(":Directory {0} was not found, Error Message:", path, unauthorizedAccessException.ToString());
            }
        }

        private XmlNode GetXmlNode(FileSystemInfo fileSystemInfo, String elementName)
        {
            var node = xmlDocument.CreateElement(elementName);

            //Add CreateTime attribute
            var createTime = xmlDocument.CreateAttribute("CreateTime");
            createTime.Value = fileSystemInfo.CreationTime.ToString();
            node.Attributes.Append(createTime);

            //Add ModifyTime attribute
            var modifyTime = xmlDocument.CreateAttribute("ModifyTime");
            modifyTime.Value = fileSystemInfo.LastWriteTime.ToString();
            node.Attributes.Append(modifyTime);

            //Set Inner text of this node
            node.InnerText = fileSystemInfo.Name;

            return node;
        }
    }
}
