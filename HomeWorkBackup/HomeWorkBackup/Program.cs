﻿namespace HomeWorkBackup
{
    using System;
    using System.IO;

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ILogger logger = FileLogger.GetInstance();

                Console.WriteLine("Input log file directory(Default CurrentDirectory) : ");
                String loggerPath = Console.ReadLine();
                logger.SetLoggerPath(loggerPath);

                Console.WriteLine("Input xml file directory(Default \"C:\\Windows\\Temp\\FileInfo.xml) : \"");
                String xmlPath = Console.ReadLine();
                XMLBackuper xmlBackuper = new XMLBackuper(xmlPath);

                Console.WriteLine("Input root path: ");
                String rootPath = Console.ReadLine();
                while (!Directory.Exists(rootPath))
                {
                    Console.WriteLine("Not exist, Input root path: ");
                    rootPath = Console.ReadLine();
                }
                xmlBackuper.Traversal(rootPath);

                Console.WriteLine("Input Backup directory(Default C:\\Backup\\)");
                String backDir = Console.ReadLine();
                if (backDir.Length == 0)
                {
                    backDir = @"C:\\Backup\\";
                }
                xmlBackuper.Restore(backDir);

                Console.WriteLine("See your Backup files in Path: {0}", xmlBackuper.DestinPath);
                Console.WriteLine("See your log in Path: {0}", logger.GetLoggerPath());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
