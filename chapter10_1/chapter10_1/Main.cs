using System;

namespace chapter10_1
{
	public class Myclass {
		protected string data;
		public string Data {
			set {
				data = value;
			}
		}
		
		public virtual string GetString() {
			return data;
		}
	}
	
	public class MyDerivedClass : Myclass {
		public override string GetString() {
			return "output from derived class " + base.GetString();
		}
	}
	
	class MainClass
	{
		public static void Main (string[] args)
		{
			Myclass mc = new Myclass();
			mc.Data = "NIMA";
			Console.WriteLine (mc.GetString ());
			
			MyDerivedClass mdc = new MyDerivedClass();
			mdc.Data = "LiuZhuang";
			Console.WriteLine(mdc.GetString());
			//Error, Data is Wrtiteonly
			//Console.WriteLine (mc.Data);
			
		}
	}
}
