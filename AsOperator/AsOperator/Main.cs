using System;

namespace AsOperator
{
	public class A {
	}
	public class B : A {
		public int data;
	}
	
	class MainClass
	{
		public static void Main (string[] args)
		{
			A a = new A();
			B b = a as B;
			if(b == null) {
				Console.WriteLine ("b is null");
			}
			B b2 = new B();
			b2.data = 10;
			A a2 = b2;
			B b3 = a2 as B;
			if(b3 != null) {
				Console.WriteLine ("b3 is not null");
				Console.WriteLine ("b3 is {0}", b3.data);
			}
			b2.data = 100;//b2 == b3;
			Console.WriteLine (b3.data);
		}
	}
}
