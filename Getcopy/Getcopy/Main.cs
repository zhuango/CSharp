using System;

namespace Getcopy
{
	public class MyCopyableClass {
		private int data;
		public MyCopyableClass(int Data) {
			data = Data;
		}
		public MyCopyableClass GetCopy() {
			return (MyCopyableClass) base.MemberwiseClone();
		}
		
		public void Print() {
			Console.WriteLine (data);
		}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			MyCopyableClass mcc = new MyCopyableClass(12);
			mcc.Print ();
			
			MyCopyableClass mcc2 = mcc.GetCopy ();
			mcc2.Print ();
			Console.WriteLine ("Hello World!");
		}
	}
}
