using System;
using System.Collections.Generic;
namespace ListType
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			List<string> list = new List<string>();
			
			list.Add ("NIMA");
			list.Add ("LiuZhuang");
			list.Add ("ZhangKaiLi");
			list.Sort ();
			foreach(string tmp in list) {
				Console.WriteLine (tmp);
			}
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			
			Dictionary<string , int> D = new Dictionary<string, int>();
			D.Add ("LiuZhuang", 10);
			D.Add ("ZHangKaiLI", 1000);
			D.Add ("Liuxiaocheng", 1000);
			
			//键和值，分别迭代，使用字典的Keys， Values属性
			foreach(string str in D.Keys) {
				Console.WriteLine (str);
			}
			Console.WriteLine ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			foreach(int age in D.Values) {
				Console.WriteLine (age);
			}
			Console.WriteLine ("##############################################");
		}
	}
}
