using System;

public class Test {
	private int data;
	public int Data {
		get {
			//Cearly, this is from class
			return this.data;
		}
	}
	public Test(int a) {
		data = a;
	}
	public void Print() {
		Console.WriteLine(Data);
	}
}
public class main {
	public static void Main(string[] args) {
		Test t = new Test(10);
		t.Print();
		Console.ReadKey(true);
	}
}
