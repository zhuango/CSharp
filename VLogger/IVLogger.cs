﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VFileManager.VFileCommonUtility.VLogger
{
    public enum VLoggerType
    {
        Info,
        Debug,
        Error,
        Warning,
    }

    public interface IVLogger
    {
        void Logger(VLoggerType loggerType, string format, params object[] parameters);

        void Info(string format, params object[] parameters);

        void Debug(string format, params object[] parameters);

        void Error(string format, params object[] parameters);

        void Warning(string format, params object[] parameters);
    }
}
