﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO;

namespace VFileManager.VFileCommonUtility.VLogger.Impl
{
    /// <summary>
    /// 没有解决多线程问题
    /// </summary>
    public class VFileLogger : IVLogger
    {
        private static VFileLogger mLogger = null;
        private static object mObjForLock = new object();
        private string mLoggerPath = string.Empty;

        private VFileLogger()
        {
        }

        public void EnsureLoggerProperty(string loggerPath)
        {
            mLoggerPath = loggerPath;
            if (string.IsNullOrEmpty(mLoggerPath))
            {
                mLoggerPath = Environment.CurrentDirectory;
            }
        }

        public static VFileLogger GetInstance()
        {
            if (mLogger == null)
            {
                lock (mObjForLock)
                {
                    if (mLogger == null)
                    {
                        mLogger = new VFileLogger();
                    }
                }
            }
            return mLogger;
        }

        #region IVLogger Members

        public void Logger(VLoggerType loggerType, string format, params object[] parameters)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendFormat("{0}\t{1}\t\t", DateTime.Now.ToString("dd-MM-yyyy_HH.mm.ss", DateTimeFormatInfo.InvariantInfo), loggerType);
                stringBuilder.AppendFormat(format, parameters);
                stringBuilder.AppendLine();

                using (StreamWriter writer = new StreamWriter(mLoggerPath + "Logger.log", true, Encoding.UTF8))
                {
                    writer.Write(stringBuilder.ToString());
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                //TODO
                Console.WriteLine(ex.ToString());
            }
        }

        public void Info(string format, params object[] parameters)
        {
            Logger(VLoggerType.Info, format, parameters);
        }

        public void Debug(string format, params object[] parameters)
        {
            Logger(VLoggerType.Debug, format, parameters);
        }

        public void Error(string format, params object[] parameters)
        {
            Logger(VLoggerType.Error, format, parameters);
        }

        public void Warning(string format, params object[] parameters)
        {
            Logger(VLoggerType.Warning, format, parameters);
        }

        #endregion
    }
}
