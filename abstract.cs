using System;
namespace ab {
	abstract class abC {
		abstract public void Write3();
		virtual public void Write() {
			Console.WriteLine("class abC");
		}
		public void Write2() {
			Console.WriteLine("abC");
		}
	}

	class myclass: abC {
		public override void Write3() {
			Console.WriteLine("Write3");
		}
		public void func() {
			Console.WriteLine("myclass");
		}
		public override void Write() {
			Console.WriteLine("class myclass");
		}
		//Error, there is no virtual in abC
		//public override void Write2() {
		//	Console.WriteLine("write2 myclass");
		//}
	}

	class ab {
		public static void Main(string[] args) {
			myclass mc = new myclass();
			//Error, cannot get instance of a abstract class
			//abC abc = new abC();
			mc.func();
			abC abc = new myclass();
			//Error, abC has no func() function
			//abc.func();
			abc.Write();
			abc.Write2();
			abc.Write3();

		}
	}
}
