﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ch10CardLib
{
   public class Deck
   {
      private Card[] cards;

      public Deck()
      {
         cards = new Card[52];
         for (int suitVal = 0; suitVal < 4; suitVal++)
         {
            for (int rankVal = 1; rankVal < 14; rankVal++)
            {
               cards[suitVal * 13 + rankVal - 1] = new Card((Suit)suitVal,
                                                           (Rank)rankVal);
            }
         }
      }

      public Card GetCard(int cardNum)
      {
			//0 ～ 51 有效数字，之外的都是非法数据
         if (cardNum >= 0 && cardNum <= 51)
            return cards[cardNum];
         else
            throw (new System.ArgumentOutOfRangeException("cardNum", cardNum,
                      "Value must be between 0 and 51."));
      }

      public void Shuffle()
      {
         Card[] newDeck = new Card[52];
         bool[] assigned = new bool[52];
         Random sourceGen = new Random();
         for (int i = 0; i < 52; i++)
         {
            int destCard = 0;
            bool foundCard = false;
            while (foundCard == false)
            {
			   //找到洗牌后的牌位置
               destCard = sourceGen.Next(52);
			   //如果这个位置没有使用，则退出循环,放入这个位置
               if (assigned[destCard] == false)
                  foundCard = true;
            }
            assigned[destCard] = true;
            newDeck[destCard] = cards[i];
         }
         newDeck.CopyTo(cards, 0);
      }
		public void Flush() {
			Card[] flush = new Card[5];
			bool[] visited = new bool[52];
			Random sourceGen = new Random();
			int i = 0;
				
			while(true) {
				int index = sourceGen.Next (52);
				if(visited[index] == false) {
					flush[i] = cards[index];
					visited[index] = true;
					i++;
				}
				if(i >= 5) break;
			}
			//These five cards
			for(int j = 0; j < 5; j++) {
				Console.WriteLine (cards[j].ToString());
			}
			Console.WriteLine ();
			Console.WriteLine ();
			Console.WriteLine ();
			
			
			Suit s = flush[0].suit;
			for(i = 0; i < 5; i++) {
				if(flush[i].suit != s) {
					for(int j = 0; j < 52; j++) {
						if(visited[j] != true) {
							Console.WriteLine (cards[j].ToString());
						}
					}
					Console.WriteLine("No flush");
					return ;
				}
			}
			for(int j = 0; j < 5; j++) {
				Console.WriteLine (cards[j].ToString());
			}
			Console.WriteLine ("Flush");
		}
   }
}