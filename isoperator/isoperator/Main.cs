using System;

namespace isoperator
{
	public class A {
	}
	public class B: A {
	}
	public class C: A {
	}

	
	class MainClass
	{
		public static void Main (string[] args)
		{
			//<operand> is <type> 操作符：
			//1.如果<type>是一个类类型,而<operand>也是该类型,或者它继承了该类型,或者它可以封
			//		箱到该类型中,则结果为 true。
			//2.如果<type>是一个接口类型,而<operand>也是该类型,或者它是实现该接口的类型,则结
			//		果为 true。
			//3.如果<type>是一个值类型,而<operand>也是该类型,或者它可以拆箱到该类型中,则结果
			//		为 true。

			A a = new A();
			B b = new B();
			C c = new C();
			
			if(a is A) {
				Console.WriteLine ("a is A");
			}
			if(b is A) {
				Console.WriteLine ("b is A");
			}
			if(c is A) {
				Console.WriteLine ("c is A");
			}

			A a2 = new B();
			if(a2 is B) {
				Console.WriteLine("a2 is B");
			}
			Console.WriteLine ("Hello World!");
		}
	}
}
