using System;

namespace NullableType
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//int型的a为值类型， 使用Nullable泛型使得a可以为null
			Nullable<int> a = null;
			Console.WriteLine ("a = {0}", a.ToString ());
			a = 10;
			Console.WriteLine ("a = {0}", a.ToString ());
			
			//可以使用new关键字新型引用类型的赋值
			a = new Nullable<int>();
			Console.WriteLine ("a = {0}", a.ToString ());
			
			//这不适用于引用类型,即使引用类型有一个 HasValue 属性,也不能使用这种方法,因为引用类
			//型的变量值为 null,就表示不存在对象,当然就不能通过对象来访问这个属性
			if(! a.HasValue){ 
				Console.WriteLine ("A doesn't Have Value");
			}
			
			//int? == Nullable<int>,一样的东西
			int? b = null;
			Console.WriteLine ("b = {0}", b.ToString ());
			b = 100;
			Console.WriteLine ("b = {0}", b.ToString ());
			
			int? c = new int?();
			c = 10;
			int cint = 1000;
			//使用int?类型的Value属性读取值
			Console.WriteLine ("cint + c.Value: {0}", cint = cint + c.Value);
			//进行强制类型转换			
			Console.WriteLine ("cint + (int) c: {0}", cint = cint + (int)c);
			
			int? d = new int?();
			d = 100;
			int? e = null;
			//"d ?? e" == "d == null ? e : d"
			int f = (d ?? e).Value;
			Console.WriteLine ("f = {0}", f);
		}
	}
}
